const mongoose = require("./conexion.js")
const express =require("express");
const app = express()

app.listen(3000,()=>{
    console.log("se esta escuchando el servidor en el puerto 3000")
})

const Platos = mongoose.model('platos',{
    nombre:String,
    tipo:String,
    precio:Number
})

app.put("/platos/actualizar/:nombre",(req,res)=>{
    try{

    Platos.findOne({nombre:req.params.nombre}).then( resultados=>{
        resultados.nombre = req.body.nombre;
        resultados.tipo = req.body.tipo;
        resultados.precio = req.body.precio;
        resultados.save()
        res.send("SE ACTUALIZARON EXITOSAMENTE LOS DATOS")
    });

    }catch(err){
        res.send("HUBO UN ERROR:",err);
    }
});