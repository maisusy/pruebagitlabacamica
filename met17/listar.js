const mongoose = require("./conexion.js");
const express =require("express");
const app = express();
express.urlencoded({extended:true});
app.use(express.json())
app.use(express.static(__dirname + 'conexion.js'))
const enviroment = require("./enviroment.json")

//agregar middleware de existencia de plato y aplicar a delete y pput

//forma mas sintetica
const Platos = mongoose.model('platos',{
    nombre:String,
    tipo:String,
    precio:Number
}) 

//ok
app.post("/platos/crear",(req,res)=>{
    const datos = {nombre:req.body.nombre,tipo:req.body.tipo,precio:req.body.precio}
    let dat = new Platos(datos);
    dat.save();
    res.send("SE AGREGO EXITOSAMENTE!!!!")
});


//ok
app.get("/platos/listar",(req,res)=>{
    Platos.find().then(function(resultados){
        res.send(resultados);
    })
})

//ok
app.delete("/platos/eliminar/:nombre",(req,res)=>{
    try{

    Platos.deleteOne({nombre:req.params.nombre}).then( (resultados)=>{
        res.send("SE Elimino EXITOSAMENTE")
    });

    }catch(err){
        console.log("HUBO UN ERROR:",err);
        res.send("ERROR")
    }
});

//ok
app.put("/platos/actualizar/:nombre",(req,res)=>{
    try{

    Platos.findOne({nombre:req.params.nombre}).then( resultados=>{
        resultados.nombre = req.body.nombre;
        resultados.tipo = req.body.tipo;
        resultados.precio = req.body.precio;
        resultados.save()
        res.send("SE ACTUALIZARON EXITOSAMENTE LOS DATOS")
    });

    }catch(err){
        res.send("HUBO UN ERROR:",err);
    }
});



app.listen(enviroment.development.PORT,()=>{
    console.log("se esta escuchando el servidor en el puerto 6000")
})
