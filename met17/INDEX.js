//const fetch = require("node-fetch");
/**
 const enviroment = require("./enviroment.json")

const express = require("express")
const app = express();
app.use(express.urlencoded({extended:true}));
app.use(express.json());
*/
const mongoose = require("mongoose")
mongoose.connect('mongodb://localhost:27017/test', {useNewUrlParser: true, useUnifiedTopology: true});

//definimos un esquema
schema = {nombre: String, apellido: String, edad: Number,carrera:String,turno:String}
/**
 * TIPOS DE DATOS:
    String,Number,Date, Boolean,ObjectId,Array,mas(https://mongoosejs.com/docs/schematypes.html)
 */

//Creamos un modelo con el esquema
const Alumnos = mongoose.model("Alumnos", schema);

//Definimos un objeto
const nuevo = {nombre:"PRUEBA", apellido: "prueba", edad: 18,carrera:"DESARROLLO WEB-BACK-END",turno:"PM"};
let nuevo_alumno = new Alumnos(nuevo)
nuevo_alumno.save();


//La instancia del modelo también nos permite buscar información
Alumnos.find({nombre:"PRUEBA"}).then(function (resultados){
    console.log("FUNCION FIND CON PARAMETRO?:",resultados);
});

/**
//traigo todos lo alumnos?
app.post("/alumnos/listado",(req,res)=>{
    Alumnos.find().then(function (resultados){
        res.json(resultados);
        });
})
//OPERACIONES CRUD

app.post("/alumnos/crear",(req,res)=>{
    const {nombre, apellido, edad,carrera,turno} = req.body;
    const alumno = {nombre: nombre, apellido: apellido, edad: edad,carrera:carrera,turno:turno};
    let nuevo_alumno = new Alumnos(alumno)
    nuevo_alumno.save(function(err, data){
        if(err){
            res.json("HUBO UN ERROR",err);
        }
        else{
            res.json("ALUMNO REGISTRADO PERFECTAMENTE:",data);
        }
    });
});

app.delete("/alumnos/eliminar/:id",(req,res)=>{

    Alumnos.findByIdAndDelete((req.params.id),function(err, data) {
        if(err){
            res.json("HUBO UN ERROR",err);
        }
        else{
            res.json("ALUMNO ELIMINADO");
        }
    });
});

app.put("/alumnos/actualizar/:id",(req,res)=>{
    const {nombre, apellido, edad,carrera,turno} = req.body;
    Alumnos.findByIdAndUpdate((id_alumno=req.params.id),{nombre: nombre, apellido: apellido, edad: edad,carrera:carrera,turno:turno},function(err, data) {
        if(err){
            res.json("HUBO UN ERROR",err);
        }
        else{
            res.json("ALUMNO ELIMINADO");
        }
    });
})
app.listen(enviroment.development.PORT , ()=> {
    console.log("el servidor esta escuchando el puerto nro:",enviroment.development.PORT)
})
*/