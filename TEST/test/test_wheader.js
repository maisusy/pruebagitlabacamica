const assert = require("assert")
const fetch = require("node-fetch")


describe("#CLIMA probando API", () => {

    it("API response 200",async () => {
        await fetch("http://api.openweathermap.org/data/2.5/weather?q=Bogota&appid=b9cc39f81eba1750b662e351ab328653")
        .then(response => {
            console.log(response.status)
            assert.strictEqual(response.status,200)
        })
    })

    it("API response 404",async () => {
        await fetch("http://api.openweathermap.org/data/2.5/weather?q=Bogota&appid=b9cc39f81eba1750b662e351ab328653")
        .then(response => {
            console.log(response.status)
            assert.strictEqual(response.status,404)
        })
    })

})