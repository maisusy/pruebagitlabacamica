const assert = require("assert")
const fetch = require("node-fetch")


describe("#test_api probando API", () => {

    it("API response 200",async () => {
        await fetch("https://jsonplaceholder.typicode.com/todos/1")
        .then(response => {
            console.log(response.status)
            assert.strictEqual(response.status,200)
        })
    })

    it("API response 404",async () => {
        await fetch("https://jsonplaceholder.typicode.com/todos/1zxcv")
        .then(response => {
            console.log(response.status)
            assert.strictEqual(response.status,404)
        })
    })

    it("EL user_id de a respuesta debe ser 1",async () => {
        await fetch("https://jsonplaceholder.typicode.com/todos/1")
        .then(response => response.json())
        .then(json => {
            assert.strictEqual(json.userId,1)
        })
    })

})
