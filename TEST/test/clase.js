const assert = require("assert")

const fetch = require("node-fetch")

//---------------EJEMPLO1----------------

describe('#test #clase',function() {

    describe('#indexOf()',function(){

        it('Deberia devolver -1 cuando el valor no esta en el array',function(){

            assert.strictEqual([1,2,3].indexOf(4),-1)

        })

    })

})

//----------EJEMPLO2---------------------

describe("calculos aritmeticos #clase",() => {

    before(() => {
        //inicializa bd,etc
        console.log("este bloque se ejecuta siempre antes de todos los test");
    });

    after(() => {
        //limpiar base de datos o array o variables,etc
        console.log("este bloque se ejecuta siempre despues de todos los test");
    })

    //podemos agrupar pruebas similares dentro de cada bloque
    describe("suma", () => {
        let suma = 0;

        beforeEach(() => { //es como inicializar la suma
            console.log("se ejecutara siempre antes de cada test");
            suma = 2; //me setea siempre en 2 antes de empezar cada test
        })

        it("deberia ser 2 + 3 y dar 5", () => {
            suma += 3;
            assert.strictEqual(suma,5);
        })

    })

    describe("resta", () => {
        let resta = 5;

        beforeEach(() => { //es como inicializar la resta
            console.log("se ejecutara siempre antes de cada test");
        })

        it("la resta 5 - 2,deberia ser 3", () => {
            resta -=2;
            assert.strictEqual(resta,3)
        })

        it("este me va a marcar error", () => {
            resta -=2;
            assert.strictEqual(resta,5)
        })

    })
})

//----------EJEMPLO3---------------------
