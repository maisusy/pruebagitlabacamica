const assert = require("chai").assert
const sumar = require("../calculadora").sumar
const restar = require("../calculadora").restar
const dividir = require("../calculadora").dividir
const multiplicar = require("../calculadora").multiplicar

describe("#Calculadora con assert",() => {

    describe("sumamos",() => {

        it("la suma de 2 + 3 DEBERIA SER 5 el resultado",() => {
            suma = sumar(2,3)
            assert.equal(suma,5);
        })

        it("debe ser tipo numero",() => {
            suma = sumar(2,3)
            assert.typeOf(suma,"number")
        })

    })

    describe("restamos",() => {

        it("la resta de 9 - 3 DEBERIA SER 6 el resultado",() => {
            res = restar(9,3)
            assert.equal(res,6);
        })

        it("debe ser tipo numero",() => {
            res = restar(9,3)
            assert.typeOf(res,"number")
        })
    })

    describe("multiplicamos",() => {

        it("la multiplicacion de 9 * 3 DEBERIA SER 27 el resultado",() => {
            rmult = multiplicar(9,3)
            assert.equal(rmult,27);
        })

        it("debe ser tipo numero",() => {
            rmult = multiplicar(9,3)
            assert.typeOf(rmult,"number")
        })
    })

    describe("dividimos",() => {

        it("di dividimos de 9 / 3 DEBERIA SER 3 el resultado",() => {
            div = dividir(9,3)
            assert.equal(div,3);
        })

        it("debe ser tipo numero",() => {
            div = dividir(9,3)
            assert.typeOf(div,"number")
        })
    })


})