const sumar = function (a,b) {
    return a + b
}

const restar = function (a,b) {
    return a - b 
}

const multiplicar = function (a,b) {
    return a * b
}

const dividir = function (a,b) {
    return a / b
}

const mensaje = function (a) { 
    return `el resultado es : ${a}`
}

module.exports = {
    sumar,
    restar,
    dividir,
    multiplicar,
    mensaje
}