/**DEFINICION DE DATOS-------------------------------------------- 
 * 
 * CREATE TABLE. Permite crear tablas. para eso, debes definir un 
 * nombre y los campos junto con su tipo.
 * CREATE TABLE `usuarios` (
   `nombre` varchar(100),
   `apellido` varchar(200),
   `edad` int(5)
    );


    ALTER TABLE. Permite modificar la estructura de una tabla. 
    En el siguiente ejemplo, ha cambiado el campo de nombre a 
    nombre_completo. ¡Esta operación no modificará ningún registro!
 *  ALTER TABLE usuarios CHANGE nombre nombre_completo VARCHAR(100);


    DROP TABLE. Esta instrucción elimina la tabla junto con los campos y 
    cada uno de los datos que tenga allí almacenados.
    DROP TABLE usuarios

    MANIPULACION DE DATOS----------------------------------

    SELECT. Consulta y trae información de nuestras tablas. Nos permite
    traer todos los registros de una tabla específica junto con todas
    las columnas de cada uno o utilizar el comodín * para seleccionar
    todos los campos
    SELECT * FROM entidad
    SELECT campo1, campo2, campo3 FROM entidad


    INSERT. Introduce nuevos registros en nuestra tabla. Debemos 
    introducir entre paréntesis y separado por coma todos los campos
     donde deseo insertar un dato, luego la palabra reserva VALUES y 
     nuevamente entre paréntesis los valores a insertar
    INSERT INTO usuarios (nombre, apellido, edad) VALUES (“Gustavo”, “Fernandez”, 25);
 
 
    UPDATE. Actualiza uno o varios registros en nuestra tabla.
    UPDATE usuarios SET nombre = “Marcos”, apellido=”Sandoval”
 
    DELETE. Elimina registros en nuestra tabla.
    DELETE FROM usuarios
 

    WHERE. La palabra reservada WHERE nos permite añadir condiciones a nuestros sentencias SQL para seleccionar registros según la condición que escribamos. Podemos utilizar WHERE combinado con el
    SELECT, UPDATE o DELETE
    SELECT * FROM usuarios WHERE id=1

    OPERADORES LOGICOS---------------------
    > mayor
    >= mayor o igual
    < menor
    <= menor o igual
    = igual
    != distinto

    
    */