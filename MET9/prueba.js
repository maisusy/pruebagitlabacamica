
/*Para poder leer archivos locales, NodeJS incluye un paquete especialmente dedicado a esto,
por eso no es necesario instalarlo. Lo único que deberás hacer es importarlo de
la siguiente manera:
import * as fs from 'fs';

node index.js //para iniciar el servidor

let fileData = fs.readFileSync('nombre archivo', 'utf8');//leer archivo
*/

const express = require('express')
const app = express();

app.get('/', (req, res) => {
  res.send('Hola')
});

app.listen(8000, () => {
  console.log('Example app listening on port http://127.0.0.1:8000/ !')
});