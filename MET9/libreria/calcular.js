function sumar(num1,num2) {
    let res = num1+num2
    let ver = `${num1} + ${num2} = ${res}`
    return ver
}
function restar(num1,num2) {
    let res = num1-num2
    let ver = `${num1} - ${num2} = ${res}`
    return ver
}
function multiplicar(num1,num2) {
    let res = num1*num2
    let ver = `${num1} * ${num2} = ${res}`
    return ver
}
function dividir(num1,num2) {
    if(num2==0){
        return false;
    }
    let res = num1/num2
    let ver = `${num1} / ${num2} = ${res}`
    return ver
}

exports.sumar = sumar;
exports.restar = restar;
exports.multiplicar = multiplicar;
exports.dividir = dividir;
