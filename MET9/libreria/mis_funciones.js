function sumar(num1,num2) {
    return num1+num2
}
function restar(num1,num2) {
    return num1-num2
}
function multiplicar(num1,num2) {
    return num1*num2
}
function dividir(num1,num2) {
    return num1/num2
}

function ok() {
    return "La operacion salio bien"
}

function fail() {
    return "La operacion salio mal"
}

exports.sumar = sumar;
exports.restar = restar;
exports.multiplicar = multiplicar;
exports.dividir = dividir;
exports.okey = ok;
exports.fail = fail;
