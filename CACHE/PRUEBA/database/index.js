const mongoose = require("mongoose")
mongoose.connect('mongodb://localhost:27017/MET28', 
{
    useNewUrlParser: true,
    useUnifiedTopology: true
});
module.exports = mongoose;