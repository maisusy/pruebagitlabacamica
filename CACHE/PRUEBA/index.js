const express = require("express")
const app = express()
app.use(express.urlencoded({extended: true}))
app.use(express.json())

const User = require('../models/User')

const redis = require('redis');
//por defecto toma el siguiente host:127.0.0.1 y puerto : 6379

const client = redis.createClient() 
//pero puede cambiarse si se desea agregarndo { host : "", port : ""}


client.on('error', function(error){
    console.log(error)
})


app.post("/api/nuevouser",async (req,res) => {
    const datos = new User.User(req.body)
    datos.save()
    .then( results => {
        res.status(200).json({
            msg:"Usuario creado",
            datos:results
        });
    })
    .catch( error => {
        res.status(400).json({
            msg:"ocurrio un error",
            error:error
        });
    })

})

//EJEMPLO CON API
app.get("/api/usuarios",async (req,res)=>{

    //corroboro si existen usuarios en cache o n
    client.get("usuarios",(error,rep)=>{
        if(error){//si hubo un error
            return res.status(400).json({
                msg:"ocurrio un error",
                error:error
            });
        }
        if(rep){//si existe la clave
            //devuelvo los usarios guardados en cache
            return res.status(200).json({
                msg:"listado de usuarios",
                datos : rep
            });
        }
    });

    //si no se encuentro en cache los busco en la base de datos
    const usuarios = await User.User.find();
    // ylos guardo en cache para una futurra consulta dentro del cache
    client.set("usuarios",JSON.stringify(usuarios),'EX',10*60*60,(error)=>{
        //ex:expire en segundos
        //cache o una hora
        if(error){
            return res.status(400).json({
                msg:"ocurrio un error",
                error:error
            });
        }
    }),

    //devuelvo los usuarios
    
     res.status(200).json({
        msg:"listado de usuarios",
        datos:usuarios
    });

});




app.listen(3000,()=>{
    console.log("escuchando en el puerto 3000")
})


/*
//datos para el ejemplo
let all_starks = await User.findAll({
    include : {
        mode : House,
        where : {
            nombre : "Stark"
        }
    }
})

//SET
client.set("Starks",all_starks)

//GET
client.get("Starks", (error,rep)=> {
    if(error) {
        return console.log("hubo un error")
    }
    if(rep){
        console.log(rep)
    }
})
*/