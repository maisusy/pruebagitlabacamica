const redis = require('redis');
//crear la conexion del servidor de redis
const client = redis.createClient({
    host:"localhost",
    port:6379
})
//muestra un mensaje si ocurre un error en la conexion
client.on("error",function (err){
    console.log(err)
})
//muestra un mensaje si se pudo conectar
client.on("connect",function (){
    console.log("conectado a redis!!!")
})

//tipos de datos en redis
/*
STRINGS 
HASHES(ALMACENADO DE OBJETOS)
LIST(ALMACENADO DE LISTA EN UN ITEM)
SETS(SIMILARES A LA LISTAS PERO NO PERMITE REPETIDOS)

OPERACIONES EN REDIS

EXIST()
DEL()
SET
EXPIRE
HMSET
HGETALL
GET
LRANGE
SADD
SMEMEBERS

REDIS NO SOPRTA LOS ARRAY ANIDADOS

EJEMPLO DE TIPOS DE DATOS

*/

//KEY ,VALUE    
client.set("framework","reactjs")
client.set("database","mongobd")
client.set("framework","reactjs")
client.set(["teacher","stevejobs"])

//consultar los valores usando get
client.get("framework",(err,data)=>{
    console.log("framework" + data)
})

client.get("teacher",(err,data)=>{
    console.log(data)
})


//HASHES guarda los valores (llave,valor) en objetos

client.hmset("tecnologias_hash","cache","redis",
"nonrelational","mongobd","relational","mysql")

//usamos hgetalll para devolver todos los valores almacenado del hash
//resids hashes are maps
client.hgetall("tecnologias_hash",(err,datatec)=>{
    console.log(datatec)
})
//para sacar la info de un objeto

client.hmget("tecnologias_hash","cache",(err,data)=>{
    console.log("data del objeto cache",data)
})

//neuva verdion usando hasg

client.hset("hashkey","hashtest1","some value",redis.print)

client.hmset(["hashkey","hashtest2","some other value"],redis.print)

//se puede hacer lo mismo para almacenas objetos

client.hmset("tecnologias_front",{
    "javasript" : "reactjs",
    "css":"bootstrap",
    "node":"express"

})

//LIST
//permite almacenar una lista de items y devuelve en formato de array de
client.rpush("framework_list","reactjs","angular","ember","vue","laravel",".net core")

//visualizar los valores de susa el metodo lrange

client.lrange("framework_list",0,-1,(err,list)=>{
    console.log(list)
})

//client.del("framework_list",(err,data)=>{
 //   console.log(data)
//})


//SETS

//IGUAL QUE LAS LISTAS PER NO PERMITE VALORES DUCPLICADOS

client.sadd("menu_restaurante",["botella de agua","ensalada","papitas",
"pizza","pizza","hamburguesa","ensalada"],(err,menudata)=>{
    console.log(menudata)
})

//obtener los c¿valores del set se usa el metodo smembers

client.smembers("menu_restaurante",(err,menu)=>{
    console.log({
        "menu":menu
    })
    console.log(menu)
})

//VALIDADNDO LLAVES

//exist

client.exists("framework",(err,data)=>{
    if(data === 1){
        console.log("existe unsa llave framework")
    }else{
        console.log("no existe la llave")
    }
})

//delete

client.del("framework",(err,data)=>{
    console.groupCollapsed(data)//si devuleve 1 es porque lo elimino
})

//verificar que se borro
client.exists("framework",(err,data)=>{
    if(data === 1){
        console.log("existe unsa llave framework")
    }else{
        console.log("no existe la llave")
    }
})

client.set("dias_vacaciones",20,()=>{
    client.incr("dias_vacaciones",(err,data)=>{
        console.log("dias de vacaciones aumentado a ",data)
    })
})

//eliminar la bade datos seria desde el redis-cli escribiendo FLUSHALL o FLUSHBD