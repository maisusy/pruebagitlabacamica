const express = require('express');
const app = express();
const env = require("./env.environment.json");
const puerto = env.development.PORT;


  app.post('/estudiantes', (req, res) => {
    res.status(201).send();
  });

  //Middleware a nivel route

  const esAdministrador = (req, res, next) => {
    const usuarioAdministrador = true;//true me permite ver los datos y false no
        if(usuarioAdministrador ) {
            console.log('El usuario está correctamente logueado.');
        next();
    } else {
        res.send('No está logueado');
        }
};

app.get('/estudiantes', esAdministrador, (req, res) => {
    res.send([
      {id: 1, nombre: "Lucas", edad: 37}
    ])
  });


  //CHALLENGE
 
 /* const log = (req, res, next) => {
    console.log(req.path);
    next();
}
*/

//middleware a nivel aplicacion
const logger = require("./middleware");
log = logger.log;
app.use(log);

app.get("/cursos",(req,res) => {
    res.send([
        {
            id:1,
            nombre:"Matematica"
        },
        {
            id:2,
            nombre:"Lengua"
        },
        {
            id:3,
            nombre:"Ciencias Sociales"
        },
        {
            id:4,
            nombre:"Ciencias Naturales"
        },
        {
            id:5,
            nombre:"Educacion Fisica"
        },
    ]);
 });



 app.listen(puerto, () => console.log("escuchando puerto 5000"));

 //los middleware se deben tener antes del endpoint