const express = require('express');
const app = express();
const env = require("./env.environment.json");
const puerto = env.development.PORT;
const compression = require("compression")

//para el endpoint de comprension
app.use(compression());

//middleware

//global
app.use(function(req,res,next){
    console.log(req.url);
    next();
});

app.use(function(req,res,next){
    console.log("Tiempo: ",Date.now());
    next();
});

//de aplicacion
function validarUsuario(req,res,next){
    if(req.query.usuario != "admin"){
        res.send("el usuario NO es admin");
    }else{
        next();
    }
};

/**function interceptar(req,res,next){
    res.send("interceptado");
};*/



//ENDPOINTS
app.get("/",(req,res)=>{
    res.send("hola mundo");
});

app.get("/saludo/usuario",validarUsuario,(req,res)=>{ //http://localhost:5000/saludo/usuario?usuario=admin asi va en el postman 
    let data = req.query;
    res.send("El usuario es admin");
});

/**app.get("/despedida",interceptar ,(req,res)=>{
    res.send("hola");
});*/

app.get("/comprension",(req,res)=>{
    const animal = "CangrejO    "
    res.send(animal.repeat(100))
});

app.listen(puerto, () => console.log("escuchando puerto 5000"));