/*

Criterios de Aceptación

Un usuario debe poder registrarse exitosamente en la aplicación y hacer login en la aplicación.
Los estados mínimos de los pedidos deben ser:
Pendiente: cuando el usuario inicia su pedido.
Confirmado: cuando el usuario cierra su pedido.
En preparación: cuando el administrador comienza a preparar el pedido.
Enviado: cuando el administrador envía el pedido.
Entregado: cuando el administrador entrega el pedido.
Un usuario debe poder realizar un pedido, y asignar uno o más productos a éste.
Cuando un usuario edita un pedido debe validar:
poder cambiar la cantidad de un producto seleccionado.
poder eliminar un producto seleccionado.
poder agregar un producto nuevo.
no debe poder editar el nombre y precio de un producto.
Cuando un administrador edita un producto debe:
poder cambiar el nombre del producto.
poder cambiar el precio del producto.

📝  Repasa cada una de los requerimientos del Sprint Project 1 y reflexiona: 
¿dónde te encuentras hoy y qué te falta para estar al día? 
Haz una lista con las
diferentes acciones, o tareas, que consideras necesarias para avanzar en tu Sprint
Project. Intenta también delimitar los tiempos que te llevará realizar cada tarea,
y establecer un *deadline* para cada objetivo que planteaste. ¡Esto te ayudará a
organizarte y completar tu Sprint Project a tiempo para el final del Sprint!

**Bonus track:** ¿Cuáles crees que son los principales obstáculos con los que te puedes encontrar?el tiempo 


me falta implementar  node.js
usuarios login,registro,modificacion,listado y eliminacion
Productos registro,modificacion.listado y eliminacion
pedidos,registro(usuario y producto),modificacion,listado y cancelacion

*/