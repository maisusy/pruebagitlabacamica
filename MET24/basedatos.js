const Sequelize = require("sequelize")

const ProductoModelo = require("./models/productos")
const EstadosModelo = require("./models/estados")
const DetalleModelo = require("./models/detalle")
const FDPMODELO = require("./models/formadepago")
const PedidoModelo = require("./models/pedidos")
const UsuariosModelo = require("./models/usuarios")

const sequelize = new Sequelize('sprintsql','root','',{
    host:"localhost",
    dialect:"mysql"
})



const Producto = ProductoModelo(sequelize,Sequelize)
const Estado = EstadosModelo(sequelize,Sequelize)
const detalle  = DetalleModelo(sequelize,Sequelize)
const fdp = FDPMODELO(sequelize,Sequelize)
const pedido  = PedidoModelo(sequelize,Sequelize)
const usuario = UsuariosModelo(sequelize,Sequelize)

sequelize.authenticate() 
    .then( () => { 
    console.log('Conexion Satisfactoria'); 
    }) 
    .catch( () => {
     console.log('Conexion con problemas'); 
    }); 


( async () => { 
    await sequelize.sync({ force: false }); console.log('Tabla cargada.'); 
})(); 


module.exports = {
    Producto,
    Estado,
    detalle,
    fdp,
    pedido,
    usuario
}