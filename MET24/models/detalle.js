module.exports = (sequelize,type) => {
    return sequelize.define("Detalle",{
        id : {
            type : type.INTEGER,
            primaryKey : true,
            autoIncrement : true
        },
        producto : {
            type : type.INTEGER,
            references : {
                model: "Productos",
                key : "id"
            }
        },
        cantidad : type.INTEGER
    })
}