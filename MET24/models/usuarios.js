module.exports = (sequelize,type) => {
    return sequelize.define("usuario",{
        id: {
            type : type.INTEGER,
            primaryKey : true,
            autoIvrement : true
        },
        usuario : type.STRING,
        nom_ape : type.STRING,
        correo : type.STRING,
        telefono : type.INTEGER,
        direccion : type.STRING,
        contrasenia : type.STRING,
        admin : type.BOOLEAN,
        status : type.BOOLEAN
    })
}