module.exports  = (sequelize,type)=>{
    return sequelize.define("formadepago",{

        id : {
            type : type.INTEGER,
            primaryKey : true,
            autoIncrement : true
        },
        nombre : type.STRING
    }

    );
}