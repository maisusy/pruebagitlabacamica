module.exports = (sequelize,type)=>{
    return sequelize.define("Producto",
    {
        id:{
            type: type.INTEGER,
            primaryKey : true,
            autoIncrement : true
        },
        nombre : type.STRING,
        precio : type.INTEGER
    });
    
}