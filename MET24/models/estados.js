module.exports = (sequelize,type)=>{
    return sequelize.define("estados",{
        id: {
            type : type.INTEGER,
            primaryKey : true,
            autoIncrement : true
        },
        nombre : type.STRING
    })
}