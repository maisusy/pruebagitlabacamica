module.exports = (sequelize,type)=>{
    return sequelize.define("pedidos",{
        id:{
            type : type.INTEGER,
            primaryKey : true,
            autoIncrement : true
        },
        id_estado : {
            type : type.INTEGER,
            references : {
                model : "estados",
                key: "id"
            }
        },
        hora : type.STRING,
        descripion : type.STRING,
        detalle : {
            type : type.INTEGER,
            references : {
                model: "Detalles",
                key : "id"
            }
        },
        forma_de_pago : {
            type : type.INTEGER,
            references : {
                model : "formadepagos",
                key : "id"
            }
        },
        total : type.INTEGER,
        usuario : {
            type : type.INTEGER,
            references : {
                model : "usuarios",
                key : "id"
            }
        },
        direccion : type.STRING

    })
}