const mongoose = require("../conexion");
const express = require("express");
const app = express();
app.listen(3500, function () {
  console.log("listening on 3500");
});

const Schema = require("../MODELS/posteo")


const mdlPosteo = mongoose.model('Posteo',Schema.Posteo);

function insertarPosteo() {

    const objPosteo = new mdlPosteo({
        titulo: "Hola Mundo!",
        cuerpo:"Este es el cuerpo del documento",
        fecha: new Date(),
    });
    objPosteo.comentarios.push({titulo:'Esto es un documento embebido, comentario de prueba!!!', cuerpo:"Cuerpo de prueba", fecha:new Date()});
    objPosteo.save((err) => {
        if(!err) console.log('Guardado'); 
    });
}

function eliminarPosteo() {
    mdlPosteo.findById('612018b74cdb3f45900c9599',(err, post) =>{
        if(!err) {
            post.comentarios[0].remove();
            post.save((err) =>{
                if(!err) console.log('Eliminado')
            });
        }
    });
}

function encontrarPosteo() {
    mdlPosteo.findById('6120317de4dbf35a6073976d',(err, post) =>{
        if(!err) {
            console.log('comentario',post.comentarios[0].titulo);          
        }
    });
}
//insertarPosteo();
//eliminarPosteo();
//encontrarPosteo();







