const express = require("express");
const app = express();
app.use(express.static(__dirname + 'conexion.js'))
const enviroment = require("./enviroment.json");

const cuenta = require("./routers/CuentaBanco-ruta");

app.use('/HomeBancking', cuenta);

app.listen(enviroment.development.PORT,()=>{
    console.log("escuchando el puerto:",enviroment.development.PORT)
})