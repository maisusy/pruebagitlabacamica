const mongoose = require("../conexion.js");
const express = require("express");
const app = express.Router();
express.urlencoded({extended:true});
app.use(express.json())
app.use(express.static(__dirname + 'conexion.js'))
const assert = require("assert")

const Schema = require("../MODELS/cuentabancaria")

const CuentaBancaria = mongoose.model("Cuenta",Schema.cuenta);

const m = require("../MIDDLEWARE/EmailsDuplicados")

app.get("/listar",(req,res)=>{

    CuentaBancaria.find().then((result,error)=>{
        if(error){
            res.json("HA OCURRIDO UN ERROR");
            throw new Error("HA OCURRIDO UN ERROR");
        }else{
            res.json(result);
        }
    }).catch((err)=>{
        console.log(err)
        res.json("HA OCURRIDO UN ERROR");
    })

})

app.post("/crear",m.VerificarEmailNoDuplicado,(req,res)=>{

    console.log(req.body)
    const {nombre,email,apellido,saldoinicial}=req.body;
    const datos = {
        nombre:nombre,
        apellido:apellido,
        email:email,
        saldo:saldoinicial
    }

    let dat = new CuentaBancaria(datos);
   
    dat.save((error)=>{
        if(error){
            console.log(error)
            res.json("HA OCURRIDO UN ERROR")
        }else{
            res.json("SE CREO EXITOSAMENTE LA CUENTA")
        }
    });

})

app.put("/modificar/:email",(req,res)=>{
    const {monto}=req.body;
    try{
        CuentaBancaria.findOne({email:req.params.email}).then((result,error)=>{
            if(error){
                throw new Error("ha ocurrido un error")
            }else{
            result.saldo = result.saldo + monto;
            result.save((error)=>{
                if(error){
                    res.json("HA OCURRIDO UN ERROR",error)
                }else{
                    res.json("SE MODIFICO EXITOSAMENTE EL MONTO")
                }
            });
                }
            });
    }catch(err){
        res.json("HA OCURRIDO UN ERROR",error)
        console.log(err);
    }
})


app.post("/transferir/:email",(req,res)=>{
    const {email_destino,monto} = req.body;

    async function TRANSFERIR(){

        const session = await mongoose.startSession();
        session.startTransaction()

        //verifico que exista la cuenta de origen
        let cuenta_encontra = await CuentaBancaria.findOne({email:req.params.email}).then((result)=>{
            console.log("TODO OKEY 1(88)")
        }).catch(err=>{
            console.log("ERROR (90)",err)
            await session.abortTransaction()
        })

        assert.ok(!cuenta_encontra)

        //verifico que exista la cuenta de destino
        await CuentaBancaria.findOne({email:email_destino}).then((result)=>{
            console.log("TODO OKEY 2(96)")
        }).catch(err=>{
            console.log("ERROR (98)",err)
            await session.abortTransaction()
            
        })

        //verifico que la cuenta de origen tenga fondos suficientes
        await CuentaBancaria.findOne({email:req.params.email}).then((result)=>{
            if(result.saldo<monto){
                console.log("EL SALDO ES INSUFICIENTE(105)")
                await session.abortTransaction()
                
            }else{
                console.log("TODO OK 3 (108)")
            }
        }).catch(err=>{
            console.log("ERROR (111)",err)
            await session.abortTransaction()
            
        })

        //resto el monto de la cuenta de origen
        await CuentaBancaria.findOne({email:req.params.email}).then((result)=>{
            result.saldo = result.saldo - monto;
            result.save((error)=>{
                if(error){
                    console.log("HA OCURRIDO UN ERROR (120)",error)
                    await session.abortTransaction()
                }else{
                    console.log("TODO OK (122)")
                }
            });
        }).catch(err=>{
            console.log("ERROR (126)",err)
            await session.abortTransaction()
            
        })

        await CuentaBancaria.findOne({email:email_destino}).then((result)=>{
            result.saldo = result.saldo + monto;
            result.save((error)=>{
                if(error){
                    console.log("HA OCURRIDO UN ERROR(134)",error)
                    await session.abortTransaction()
                }else{
                    res.json("SE HA TRANSFERIDO EXITOSAMENTE(136)")
            await session.abortTransaction()
                }
            });
        }).catch(err=>{
            console.log("ERROR (111)",err)
            await session.abortTransaction()
            
        })        

        session.commitTransaction()
        session.endSession()
    }
    
    TRANSFERIR();

})


module.exports = app;