const mongoose = require("../conexion.js");

const Schema = require("../MODELS/cuentabancaria")

const CuentaBancaria = mongoose.model("Cuenta",Schema.cuenta);

function VerificarEmailNoDuplicado(req,res,next){
    CuentaBancaria.findOne({email:req.body.email}).then((result,error)=>{
        if(error){
            throw new Error("HA OCURRIDO UN ERROR");
        }else{
            if(!result){
                next()
            }else{
                res.json("YA EXISTE LA CUENTA CON EL EMAIL")
            }
        }
    }).catch((err)=>{
        res.json("HA OCURRIDO UN ERROR")
        console.log(err)
    })
};

exports.VerificarEmailNoDuplicado = VerificarEmailNoDuplicado;