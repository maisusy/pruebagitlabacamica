/**
    startSession(): las transacciones están asociadas a una sesión, es por ello que siempre necesitaremos abrir una sesión.
    endSession(): cuando cerremos la sesión, si hay alguna transacción ejecutándose, esta se abortará.
    Session.startTransaction(): este método nos sirve para empezar la transacción dentro de una sesión.
    Session.commitTransaction(): con este método confirmaremos los cambios que hayamos realizado dentro de la transacción.
    Session.abortTransaction(): aborta la ejecución de la transacción y, por lo tanto, no graba las modificaciones hechas a lo largo de la transacción.
 */ 
const { Schema } = require("mongoose");
const db = require("../conexion.js");
var assert = require('assert');

run().catch(err => console.log(err));

async function run() {

await db.connection.dropDatabase();

//TRANSACCIONES---------------------------------------------------------------------------

//Commit-----------------------------------------
const Usuarios = db.model("User",new Schema({nombre:String}));


const session = await db.startSession()
session.startTransaction()

//el metodo create() es parte de la trnasaccion por la opcion session
await Usuarios.create([{nombre:"test"},{session:session}]);

//el documento no se va a ver hasta que se haga el commit
const dat = {nombre:"test"}
let datos = new Usuarios(dat)
datos.save()

//esta operacion va a devolver el documento ya que es parte de la transaccion con la opcion session

doc = await Usuarios.findOne({nombre:"test"}).session(session);
assert.ok(doc,"ok")
//una vez que se commitea la transaccion el documento es visible fuera de ella
await session.commitTransaction();

doc =  await Usuarios.findOne({nombre:"test"});
assert.ok(doc,"ok")

console.log("fin")
session.endSession();
}

/**

//ABORT------------------------------

session.startTransaction();
// se crean dos usuarios dentro de esta transaccion
await Usuarios.create([{nombre:"test1"},{session:session}]);
await Usuarios.create([{nombre:"test2"},{session:session}]);

// al indicarle que aborte
await session.abortTransaction();

//ningun documento persiste en la BD
const count = await Usuarios.countDocuments();
//asi que la cantidad es 0
assert.strictEqual(count,0);

session.endSession();
}

*/

