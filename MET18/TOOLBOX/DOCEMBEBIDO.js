const db = require("../conexion.js");

//DOCUMENTOS EMBEBIDOS-----------------------------------------------------------------------------


const Schema = require("../MODELS/posteo")

const Posteo = db.model("Posteo",Schema.Posteo);
//creo un posteo
var post = new Posteo();
//creo un comentario dentro del posteo
post.comentarios.push({titulo:"esto es un comentario"});

post.save(function(err){
    if(!err)console.log("guardado")
});

/**

//encontrar un documento embebido------------------
posteo.findById(myid,function(err,post){
    if(!err){
        post.comentario[0].remove();
        post.save(function(err){
            //instrucciones
        })
    }
})

//eliminar un documento embebido-------
post.comentario.id(my_id).remove();
post.save(function(err){
    //documento con 'my_id'eliminado!
})
 */