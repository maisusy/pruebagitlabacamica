const mongoose = require("../conexion");
const { Schema } = mongoose;

const Comentario = new Schema({
    titulo:String,
    cuerpo:String,
    fecha:Date
});

exports.comentario = Comentario;