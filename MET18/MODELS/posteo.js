const mongoose = require("../conexion");
const { Schema } = mongoose;
const Comentario = require("./comentarios")

const Posteo = new Schema({
    autor:mongoose.ObjectId,
    titulo: String,
    cuerpo:String,
    fecha: Date,
    comentarios:[Comentario],//documento embebido se añade en forma de array
    meta :{
        votos:Number,
        favs: Number
    }
});

exports.Posteo = Posteo;