const mongoose = require("../conexion");
const { Schema } = mongoose;

const cuenta = new Schema({
    nombre:String,
    apellido:String,
    email:String,
    saldo:Number
})

exports.cuenta = cuenta;