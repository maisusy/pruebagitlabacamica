//npm i express
var express = require("express")

var server = express();


//server.METHOD(PATH, HANDLER);

const env = require("./env.environment.json")
//const envnode = process.env.NODE_ENV || "development";
//const puerto = env[envnode]
const puerto = env.development.PORT;

server.use(express.json());

server.use(express.urlencoded({extended:true}));


server.listen(puerto, function(){
    console.log(puerto);
});

//enpoint de  saludos
server.get("/",function(req,res){
    res.json("hola mundo");
});

server.get("/saludo",function(req,res){
    res.json("hola mundo");
});

server.get("/saludo/:nombre",function(req,res){
    res.json("hola mundo"+req.params.nombre);
});

/** 
//get > obtener info
server.get("/saludos" , function(req,res){
    console.log("hola mundo");
    res.json("saludos")
})

server.get("/saludos/status/" , function(req,res){
    console.log("hola mundo");
    res.status(200).json("saludos ok")
    //o  res.json("saludos ok")
})
*/

//CONFIGURACION DEL USUARIO
let usuario = {
    nombre:"",
    apellido:"",
}

let respuesta = {
    error:"",
    codigo:"",
    mensaje:"",
    respuesta:"",
}

let datos = [];

//USUARIOS
  //CREACION DE USUARIOS
  server.post("/usuario", function (req, res) {
    console.table(req.body);
    if (!req.body.nombre || !req.body.apellido) {
      respuesta = {
        error: true,
        codigo: 502,
        mensaje: "El campo nombre y apellido son requeridos",
      };
    } else {
      if (req.body.nombre !== "" || req.body.apellido !== "") {
        usuario = {
          nombre: req.body.nombre,
          apellido: req.body.apellido,
        };
        respuesta = {
          error: false,
          codigo: 200,
          mensaje: "Usuario creado",
          respuesta: usuario,
        };
        datos.push(usuario);
        console.log(datos);
      }
    }
    res.send(respuesta);
  });

  server.delete("/usuario", function (req, res) {
    console.table(req.body);
    if (!req.body.nombre) {
      respuesta = {
        error: true,
        codigo: 502,
        mensaje: "El campo nombre es requerido",
      };
    } else {
      if (req.body.nombre !== "") {
        let nombre_data = req.body.nombre;
        let filter_data = datos.filter((x) => x.nombre !== nombre_data);
        datos = [...filter_data];
        console.log(datos);
      }
      respuesta = {
        error: false,
        codigo: 200,
        mensaje: "Usuario borrado",
        respuesta: usuario,
      };
    }
    res.send(respuesta);
  });

  //actualizar
  server.put("/usuario", function (req, res) {
    if (!req.body.nombre || !req.body.apellido) {
      respuesta = {
        error: true,
        codigo: 502,
        mensaje: "El campo nombre y apellido son requeridos",
      };
    } else {
      if (req.body.nombre !== "" || req.body.nombre !== "") {
        let nombre_req = req.body.nombre;
        let apellido_req = req.body.apellido;
        datos.map((item) => {
          if (item.nombre === nombre_req) {
            item.apellido = apellido_req;
          }
        });
        respuesta = {
          error: false,
          codigo: 200,
          mensaje: "Usuario actualizado",
          respuesta: usuario,
        };
      }
    }
    res.send(respuesta);
  });

  server.get("/usuario", function (req, res) {
    respuesta = {
      error: false,
      codigo: 200,
      mensaje: "Lista de Usuarios",
      usuarios: datos,
    };
    res.send(respuesta);
  });

/**
 
 const express = require("express");
const app = express();
//ubicamos el puerto del servidor con las variables de entorno
const env = require("./variables.entorno.json");
const node_env = process.env.NODE_ENV || "development";
const puerto = env[node_env];
//aceptar valores json
//Este método se llama como middleware
app.use(express.json());
//es un método incorporado en express para reconocer el objeto de solicitud entrante como objeto JSON.
//Este método se llama como middleware
app.use(express.urlencoded({ extended: true }));
//configuramos los endpoints con los verbos HTTP : GET, POST, PUT, DELETE
//ESTRUCTURA
//
//app.nombre_verbo('ruta_del_endpoint', callback_funcion);
//ejm.ruta_del_endpoint('/usuarios')
//ejm.ruta_del_endpoint('/carros')
//ejem.callback_funcion => function(req, res)
/*
GET = Obtener
POST = Crear
PUT = Actualizar
DELETE = Eliminar

//array para almecenar datos
let usuario = {
    nombre: "",
    apellido: "",
  };
  let respuesta = {
    error: "",
    codigo: "",
    mensaje: "",
  };
  let datos = [];
  //get >obtener informacion
  app.get("/", function (req, res) {
    console.log("hola mundo:");
    res.json("Hola Mundo");
  });
  //get >obtener informacion
  //nombre del recurso: saludo
  app.get("/saludo", function (req, res) {  
    res.json("Hola sin query params");
  });
  //get >obtener informacion
  ///saludo/luis
  app.get("/saludo/:nombre", function (req, res) {  
    res.json("Hola bienvenido " + req.params.nombre);
  });
  //codigos status:200 or 201 son OK
  app.get("saludo/status/", function (req, res) {
    res.json("Saludos ok");
  });
  //post >creacion
  app.post("/saludo/", function (req, res) {
    res.json("Saludos desde el método POST");
  });
  //PUT >actualizar
  app.put("/saludo/", function (req, res) {
    res.json("Saludos desde el método PUT");
  });
  //DELETE >borrar
  app.delete("/saludo/", function (req, res) {
    res.json("Saludos desde el método DELETE");
  });
  //USUARIOS
  //CREACION DE USUARIOS
  app.post("/usuario", function (req, res) {
    console.table(req.body);
    if (!req.body.nombre || !req.body.apellido) {
      respuesta = {
        error: true,
        codigo: 502,
        mensaje: "El campo nombre y apellido son requeridos",
      };
    } else {
      if (req.body.nombre !== "" || req.body.apellido !== "") {
        usuario = {
          nombre: req.body.nombre,
          apellido: req.body.apellido,
        };
        respuesta = {
          error: false,
          codigo: 200,
          mensaje: "Usuario creado",
          respuesta: usuario,
        };
        datos.push(usuario);
        console.log(datos);
      }
    }
    res.send(respuesta);
  });
  app.delete("/usuario", function (req, res) {
    console.table(req.body);
    if (!req.body.nombre) {
      respuesta = {
        error: true,
        codigo: 502,
        mensaje: "El campo nombre es requerido",
      };
    } else {
      if (req.body.nombre !== "") {
        let nombre_data = req.body.nombre;
        let filter_data = datos.filter((x) => x.nombre !== nombre_data);
        datos = [...filter_data];
        console.log(datos);
      }
      respuesta = {
        error: false,
        codigo: 200,
        mensaje: "Usuario borrado",
        respuesta: usuario,
      };
    }
    res.send(respuesta);
  });
  //actualizar
  app.put("/usuario", function (req, res) {
    if (!req.body.nombre || !req.body.apellido) {
      respuesta = {
        error: true,
        codigo: 502,
        mensaje: "El campo nombre y apellido son requeridos",
      };
    } else {
      if (req.body.nombre !== "" || req.body.nombre !== "") {
        let nombre_req = req.body.nombre;
        let apellido_req = req.body.apellido;
        datos.map((item) => {
          if (item.nombre === nombre_req) {
            item.apellido = apellido_req;
          }
        });
        respuesta = {
          error: false,
          codigo: 200,
          mensaje: "Usuario actualizado",
          respuesta: usuario,
        };
      }
    }
    res.send(respuesta);
  });
  app.get("/usuarios", function (req, res) {
    respuesta = {
      error: false,
      codigo: 200,
      mensaje: "Lista de Usuarios",
      usuarios: datos,
    };
    res.send(respuesta);
  });
  //configuramos expreess para realizar el llamado
  //todas las peticiones sobre mi servidor van a recibirse sobre este puerto 3000
  app.listen(puerto, function () {
    console.log("Server corriendo sobre el puerto " + puerto.port);
  });
 */
