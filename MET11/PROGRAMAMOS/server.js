//npm i express
var express = require("express")

var server = express();

const env = require("./env.environment.json")
//const envnode = process.env.NODE_ENV || "development";
//const puerto = env[envnode]
const puerto = env.development.PORT;


let telefonos = [{
    marca: "Samsung",
    modelo: "S10",
    gama: "Alta",
    pantalla: "19:9",
    sistema_operativo: "Android",
    precio: "1000"
},{
    marca: "Iphone",
    modelo: "12 Pro",
    gama: "Alta",
    pantalla: "OLED",
    sistema_operativo: "iOS",
    precio: "1200"
},{
    marca: "Nokia",
    modelo: "1100",
    gama: "Baja",
    pantalla: "LCD",
    sistema_operativo: "Series 40",
    precio: "150"
},{
    marca: "Xiaomi",
    modelo: "Mi 9T Pro",
    gama: "Media",
    pantalla: "LCD",
    sistema_operativo: "Android 9 Pie",
    precio: "450"
}]

server.get("/normal",function(req,res){
    res.json(telefonos);
})

server.get("/medio",function(req,res){
    res.json(telefonos.splice(0,(telefonos.length/2)));
})

server.listen(puerto, function(){
    console.log(puerto);
});


//GRUPAL
let respuesta = {
    marca: "",
    modelo: "",
    gama: "",
    pantalla: "",
    sistema_operativo: "",
    precio: ""
}

//el  menor
server.get("/menor",function(req,res){
    let precio = 9999999;

    telefonos.forEach((item) => {
        let p = parseInt(item.precio)
        if ( p < precio) {
            respuesta = {
                marca: item.marca,
                modelo: item.modelo,
                gama: item.gama,
                pantalla: item.pantalla,
                sistema_operativo: item.sistema_operativo,
                precio: item.precio,
            };
            precio = item.precio;
        }
      });

    res.json(respuesta);
});

//mas alto
server.get("/mayor",function(req,res){
    let precio = -9999999;

    telefonos.forEach((item) => {
        let p = parseInt(item.precio)
        if ( p > precio) {
            respuesta = {
                marca: item.marca,
                modelo: item.modelo,
                gama: item.gama,
                pantalla: item.pantalla,
                sistema_operativo: item.sistema_operativo,
                precio: item.precio,
            };
            precio = item.precio;
        }
      });

    res.json(respuesta);
});

//agrupar por gama
var alta = [];
var baja = [];
var media = [];

let respueta

server.get("/gama",function(req,res){

    telefonos.forEach((item) => {
        if (item.gama === "Baja") {
            baja.push(item)
        }
        if (item.gama === "Alta") {
            alta.push(item)
        }
        if (item.gama === "Media") {
            media.push(item)
        }
      });

      let respuesta = {
          alta : alta,
          media : media,
          baja : baja,
      }

    res.json(respuesta);
});

