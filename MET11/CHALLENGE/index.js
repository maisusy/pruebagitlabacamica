var express = require('express');
var app = express();

/*app.METHOD(PATH, HANDLER);
app es una instancia de express.
METHOD es un verbo HTTP.(get,delete,post,put)
PATH es un endpoint.
HANDLER es la funciona que va a ejecutar si es la ruta a utilizar.*/

//http://localhost:3000/
app.get('/', function (req, res) {
  res.send('Hola mundo');
});

//http://localhost:3000/saludo
app.get('/saludo', function (req, res) {
    res.send("prueba");
  });

app.listen(3000, function () {
  console.log('Escuchando el puerto 3000!');
});

/*¿Te acuerdas del ejercicio de clases, donde creaste un array
 con el nombre de tus compañeros? Bueno, vamos a retomar ese ejemplo, 
 y en vez de imprimir el array en consola, vamos a devolverlo por una API.
  Deberías crear una ruta que devuelva una lista con el nombre de tus
   compañeros/as. ¿Te animas?*/

var alumnos = ["sara","walter","aleandro","gabriel","roberto","antonio","Zamanta"]

//http://localhost:3000/alumnos
app.get('/alumnos', function (req, res) {
    res.send(alumnos);
});

app.post('/alumnos', function (req, res) {
    //hago las validadciones
    //if(estatodo bien?){res.senf("todo ok")
    //}else{res.send(todo mal no tenes autorizacion bro)}
    res.send("Alumno agregado");
});

app.delete('/alumnos', function (req, res) {
    res.send("Alumno eliminado");
});

app.put('/alumnos', function (req, res) {
    res.send("Alumno actualizado");
});

