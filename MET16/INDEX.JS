/**const fetch = require("node-fetch");
var jsdom = require("jsdom");
var JSDOM = jsdom.JSDOM;
global.document = new JSDOM("./principal.html").window.document;

console.log("div:",div)

let promesa = new Promise((resolve,reject) =>{
    fetch("https://api.github.com/users/andrew")
        .then((response)=> response.json())
        .then( data => resolve(data))
        .catch((err)=> reject(err))
})

promesa.then((result)=> {
    console.log("response,todo okey")
})

promesa.catch((error)=> {
    console.log("hubo un error")
})

//siempre devuelve una promesa
async function obtenerUsuario(){
    const response = await fetch("https://api.github.com/users/andrew")
    const data =await response.json();
    if(response.status === 200){
        return data;
    }else{
        console.log(new Error("se presento un error"));
    }
}

//console.log(obtenerUsuario())//si lo llamamos asi,solo nos devolvera que esta pendiente

obtenerUsuario().then((data) => {
    console.log("datitos del usuario");
}).catch((error)=> {
    console.log("EL USUARIO ES INEXISTENTE",error);
});
*/

let div = document.getElementById("Div");
async function obtenerpeliculas(){
    const response = await fetch("http://www.omdbapi.com/?t=angry birds&apikey=2ba95592");
    const data = await response.json();
    if(data){
        return data
    }else{
        return "mensaje errror"
    }
}

obtenerpeliculas().then((data) => { 
    //imagen
    let peliImage = document.createElement("img");
    peliImage.setAttribute("src",data.Poster);
    peliImage.style.width = "300px";
    //titulo
    let peliTitulo = document.createElement("h1");
    peliTitulo.setAttribute("id","titulo");
    peliTitulo.innerHTML = data.Title;
    //descripcion 
    let peliDescripcion = document.createElement("p");
    peliDescripcion.setAttribute("id","Descripcion");
    peliDescripcion.innerHTML = data.Plot;
    //añadimos al div
    div.appendChild(peliImage);
    div.appendChild(peliTitulo);
    div.appendChild(peliDescripcion);
}).catch((error)=> {
    console.log(error);
});



/**
//ARRAY FUNCTION
let sum = (num1,num2) => num1+num2;

console.log("la suma es ",sum(5,5))
//funcion pura
let cuadrado = num => num*num
console.log("el cuadrado es",cuadrado(4));

let mensaje = () => "hola esta es una funcion si parametros"

console.log(mensaje())

//callack
function primera(callback){
    console.log("paso la num 1")
    callback();
}

function tercera(){
    console.log("paso la 3");
}

function segunda(){
    console.log("paso la 2");
}

primera(segunda)
primera(tercera)
 */