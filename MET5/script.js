let autos = ["PEUGEOT","FERRARI","HYUNDAI","torino"];

console.log("ARRAY:",autos);
//el .length() nos trae la cantidad de elementos de un autos
console.log("ARRAY.LENGTH:",autos.length);
// unshift() sirve para añadir un elemento al principio al indice 0
autos.unshift("fiat","audi");
console.log("UNSHIFT(12)",autos);
// push() anade un elemento al final del indice autos.length
autos.push("mercedez","toyota","Mercuty 47");
console.log("PUSH(34)",autos);
//con coma agregregas mas de un elemento

//IndexOF()
console.log("INDEX DE FIAT",autos.indexOf("fiat"));//SI NO LO ENCUENTRA DEVUELVE -1

//FIND() busca una funcion y dentro de esta busca un elemento

//shift()
console.log("eliminar 1er elemento",autos.shift());

//pop()

console.log("eliminar ultimo elemento",autos.pop());

//splice(indice,1,"opcional");
console.log("CORTA EL ELEMENTO QUE QUERAMOS:",autos.splice(0,2));
console.log(autos);

//AGREGO CON SPLICE()
autos.splice(3,0,"TORINO","FORD RANGER");
console.log(autos);


//ELIMINO CON SPLICE()
autos.splice(3,1);
console.log(autos);

var frutas = ["mango","mazana","pera","mandarina"];
document.getElementById("frutas").innerHTML = frutas;

function agregarfrutas(){
    frutas.splice(2,0,"kiwi","banana")
    document.getElementById("frutas").innerHTML = frutas;
}

function eliminarfrutas(){
    frutas.splice(2,2,"moras","sandia")
    document.getElementById("frutas").innerHTML = frutas;
}


console.log("FOR--------");

for(var i = 0 ; i < frutas.length;i++){
    console.log("indice:"+i+" fruta:"+frutas[i]);
}

console.log("WHILE--------");
var indice = 0;
while(indice < frutas.length){
    console.log("indice:"+indice+" fruta:"+frutas[indice]);
    indice++;
}

