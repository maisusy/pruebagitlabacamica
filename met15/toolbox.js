


//TOOLBOX
//metodo sincronico
console.log("¿Hola ");

console.log("cómo ");

console.log("están? ");

//metodo asincronico
console.log("¿Hola ");
console.log("cómo ");
// Realizar una búsqueda en Google (ya veremos las instrucciones)
console.log("están?");

//ejemplooo
function saludar(callback) {
    callback();
}
function bienvenida(){
    console.log("Hola mundo")
}
function adios(){
        console.log("Adios mundo")
}
//saludar(bienvenida())
saludar(function (){ 
    console.log("Saludo intermedio") 
    })

/*cenar_risotto = creo una promesa {
    //ejecutó mis instrucciones
    //ir al supermercado
    //comprar ingredientes
    //pagar en la caja
    //volver a casa
    //cocinar
    if(todos los ingredientes? && pude preparar el risotto?){
        promesa exitosa
}else{
        promesa rechazada
}
}*/

//cenar_risotto.exitosa( callback () { estoy cenando risotto } )
//cenar_risotto.rechazada( callback () { debo preparar otra cena } )

let mi_promesa = new Promise((resolve, reject) => {
    const number = Math.floor(Math.random() * 5);
  if(number > 0){
    resolve((number % 2)? "es impar" : "es par");
  }else{
    reject("es cero")
  }
});

mi_promesa
    .then(number => console.log(number))
    .catch(error => console.error(error));




