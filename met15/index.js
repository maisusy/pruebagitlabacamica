
//CLASEEE

const promesa_numero = new Promise(function (resolve,reject) {
    const numero = Math.floor(Math.random() * 10);
    setTimeout(()=> {
        numero > 4 ? resolve(numero): reject(new Error("menor a 4:"+numero));
    },2000);
    
})
promesa_numero
    .then(numero => console.log(numero))
    .catch(error => console.log(error))

function sincronico() {
    console.log("hola")
    console.log("como")
    console.log("estas?")
}

function asincronico() {
    
    console.log("hola")
    setTimeout(()=>{
        console.log("como")
    },1000);
    setTimeout(()=>{
        console.log("estas?")
    },4000);
}

//promesas PARA QUE ENTRE EN RESOLVE
let promesa_ejecucion =new Promise((resolve,reject)=>{

    setTimeout(()=>{
        resolve("promesa resuelta en 1 segundo");
    },1000);

    setTimeout(()=>{
        reject("promesa rechazada");
    },1500);

})
//atrapamos la primesa usando then y catch
promesa_ejecucion.then((mensaje)=>{
    console.time("execution")
    console.log("respuesta es:",mensaje)
    console.timeEnd("execution")
}).catch((errormensaje)=>{
    console.log("error",errormensaje);
})


//promesas PARA QUE ENTRE EN REJECT
let promesa_rechazada =new Promise((resolve,reject)=>{

    setTimeout(()=>{
        resolve("promesa resuelta en 1 segundo");
    },1000);

    setTimeout(()=>{
        reject("promesa rechazada");
    },500);

})
//atrapamos la primesa usando then y catch
promesa_rechazada.then((mensaje)=>{
    console.time("execution")
    console.log("respuesta es:",mensaje)
    console.log(timeEnd("execution"))
}).catch((errormensaje)=>{
    console.log("error",errormensaje);
})

//promesa pendientee
let promesa_pendiente = new Promise((resolve,reject)=>{
    console.log("pendientes!!!!");
    setTimeout(()=>{
        if(!true){
            resolve("promesa resuelta");
        }else{
            reject("promesa rechazada");
        }
    },3500);
})

promesa_pendiente.then((succesmesagge)=>{
    console.log("respuesta de la promesa:"+succesmesagge);
}).catch((errormenssage)=>{
    console.log("error de promesa :",errormenssage)
})


let divimage = document.getElementById("imgDog");

function getDogImage(url){
    console.log("pendiente para la imagen");
    fetch(url)
        .then(response => response.json())
        .then(json => {
            let dogImage = document.createElement("img");
            dogImage.setAttribute("src",json.message);
            dogImage.style.width = "300px";
            divimage.appendChild(dogImage);
            console.log("imagen cargada");
        })
        .catch(err => {
            console.log("fallo la comunicacion al server: ",err);
        });
}

getDogImage("https://dog.ceo/api/breeds/image/random")

sincronico();
asincronico();




