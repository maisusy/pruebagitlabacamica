const jwt = require("jsonwebtoken")
const helmet = require('helmet')
const express = require("express")

const informacion = {
    id:1,
    nombre:"maria la del barrio"
}
const firma = "mi_pass_secreta" 

const token = jwt.sign(informacion,firma)
console.log(token)
const decodificado = jwt.verify(token,firma)
console.log(decodificado)


const app = express();
app.use(express.urlencoded({extensions: true}))
app.use(express.json())
app.use(helmet())


const validarUsuarioContrasenia = (usuario,contrasenia)=>{
    try{

    const claves = [
            {
                usuario:"luis",
                contrasenia:"1234567"
            },
            {
                usuario:"maria",
                contrasenia:"123"
            }
        ];

        const valido = claves.filter( x => x.contrasenia === contrasenia && x.usuario === usuario)
        if(valido.length > 0 ){
            return true
        }else{
            return false
        }
    } catch (error) {
        res.send(error)
    }
};

const autenticarusuario = (req,res,next) => {
    try{
        const token = req.headers.authorization.split(' ')[1]
        console.log(token)
        const verificarToken = jwt.verify(token,firma)
        if(verificarToken){
            req.body = verificarToken;
            return next()
        }else{
            res.send("error en el login")
        }
    } catch (e){
        res.json({error:"error al validadr usuario"})
    }
        
}


app.post("/login",(req,res)=>{
    const {usuario,contrasenia} = req.body
    const validado = validarUsuarioContrasenia(usuario,contrasenia)

    if(!validado){
        res.json({error:"usuarios o contraseña incorrecta"})
        return
    }

    const token = jwt.sign({
        usuario,
    },firma)

    res.json({token})
})


app.post("/seguro",autenticarusuario,(req,res)=>{
    res.send(`esta es una pagina autenticada hola ${req.body.usuario}`)

})




app.listen(3000,()=>{
    console.log("listening on port 3000")
})