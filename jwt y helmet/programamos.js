const jwt = require("jsonwebtoken")
const firma = "clavecita"
const informacion = {
    id:1,
    user:"admin",
    admin:true,
    nombre:"susana",
    apellido:"martinez",
    fecha_nacimiento: new Date("06/06/2001"),
    mascotas : [
        {   
            id:1,
            tipo:"gato",
            nombre:"piper"
        },
        {   
            id:2,
            tipo:"roca",
            nombre:"roqui"
        }
    ]
}

const token = jwt.sign(informacion,firma)
console.log(token)

const descodificado = jwt.verify(token,firma)
console.log(descodificado)
