const env = require("./enviroment.json");
const puerto = env.development.PORT;

const express = require("express");
const server = express();
server.use(express.urlencoded({extended:true}));
server.use(express.json())

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: 'prueba acamica swagger',
        version: '1.0.0'
      }
    },
    apis: ['./server.js'],
}

const swaggerDocs = swaggerJsDoc(swaggerOptions);

server.use('/api-docs',swaggerUI.serve,swaggerUI.setup(swaggerDocs));//localhost:3000/api-docs


/**
 * @swagger
 * /estudiantes:
 *  post:
 *   description: Crea un nuevo estudiante
 *   parameters:
 *   - name: nombre
 *     type: string
 *     description: Nombre del estudiante
 *     in: formData
 *     required: true
 *   - name: edad
 *     description: Edad del estudiante
 *     in: formData
 *     required: true
 *     type: integer
 *   responses:
 *     200:
 *       description: Sucess
 *
 */
 server.post('/estudiantes', (req, res) => {
    res.status(201).send("estudiante agregado");
  });

/**
 * @swagger
 * /estudiantes:
 *  get:
 *   description: Obtiene los estudiantes
 *   responses:
 *     200:
 *       description: Sucess
 */
  server.get('/estudiantes', (req, res) => {
    res.status(201).send("Listado de estudiantes");
  });

/**
 * @swagger
 * /estudiantes:
 *  delete:
 *   description: Elimina un estudiante
 *   responses:
 *     200:
 *       description: Sucess
 */
 server.delete('/estudiantes', (req, res) => {
    res.status(201).send("estudiante eliminado");
  });

  /**
 * @swagger
 * /estudiantes:
 *  patch:
 *   description: Actualizar los estudiantes
 *   responses:
 *     200:
 *       description: Sucess
 */
   server.patch('/estudiantes', (req, res) => { //actualizacion de un objeto especifico
    res.status(201).send("actualiza un estudiante");
  });

 /**
 * @swagger
 * /estudiantes:
 *  put:
 *   description: Actualizar los estudiantes
 *   responses:
 *     200:
 *       description: Sucess
 */
  server.put('/estudiantes', (req, res) => {
    res.status(201).send("actualiza un estudiante");
  });

  
var vehiculos = [
  {
      marca:"Ford",
      modelo:"sport",
      fecha:new Date("15/05/2018"),
      cant_de_puertas:4,
      disponibilidad:true,
  },
  {
      marca:"Mercedez",
      modelo:"sport",
      fecha:new Date("28/08/2020"),
      cant_de_puertas:4,
      disponibilidad:true,
  }
]

/**
* @swagger
* /vehiculos:
* get:
*   descriprion: Listado de vehiculos
*   responses:
*     200:
*       description: Sucess
*/
server.get("/vehiculos",(req,res)=>{
  res.send(vehiculos)
})
/**
* @swagger
* /vehiculos:
* post:
*   descriprion: Agregar vehiculos
*   parameters:
*   - name: marca
*     type: string
*     description: Nombre del estudiante
*     in: formData
*     required: true
*   - name: modelo
*     type: string
*     description: Nombre del modelo
*     in: formData
*     required: true
*   - name: fecha
*     type: Date
*     description: fecha de creacion
*     in: formData
*     required: true
*   - name: cant_de_puertas
*     type: number
*     description: cantidad de puertas
*     in: formData
*     required: true
*   - name: disponibilidad
*     type: boolean
*     description: Nombre del estudiante
*     in: formData
*     required: true
*   responses:
*     200:
*       description: Sucess
* 
*/
server.post("/vehiculos",(req,res)=>{
  const{marca,modelo,fecha,cant_de_puertas,disponibilidad} = req.body;
  const vehiculo = {
      
      marca:marca,
      modelo:modelo,
      fecha:fecha,
      cant_de_puertas:cant_de_puertas,
      disponibilidad:disponibilidad,
  }
  vehiculos.push(vehiculo)
  res.send(vehiculos)
})


server.put("/vehiculos",(req,res)=>{
  res.send(vehiculos)
})

server.delete("/vehiculos",(req,res)=>{
  res.send(vehiculos)
})


server.listen(puerto,()=>console.log("escuchando en puerto 3000"));
