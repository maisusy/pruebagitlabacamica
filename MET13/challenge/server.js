const env = require("./enviroment.json");
const puerto = env.development.PORT;

const express = require("express");
const server = express();
server.use(express.urlencoded({extended:true}));
server.use(express.json())

//CHALLENGE
/**
 A la hora de llevar adelante un proyecto, no sólo es clave resolverlo técnicamente, sino que
  además es preciso hacerlo dentro de los deadlines acordados. Durante los últimos años, muchos/as
   desarrolladores/as se dedicaron a pensar, probar y sistematizar técnicas de gestión del tiempo.

📄 En [este artículo](http://www.ceolevel.com/4-tecnicas-para-estimar-pert-delphi-planning-poker-tshirt)
 encontrarás cuatro que son bastante conocidas: PERT, Delphi, Planning Poker y T-shirt. Échales un vistazo
  y elige una de ellas. Aplica alguna a tu proyecto para estimar el grado de avance.
¿Cuánto te falta para completarlo?
apenas deje hecha la base jajajaj
 */


//PRUEBA
//INTALACION DE LIBRERIAS:
//swagger-jsdoc: esta librería leerá las anotaciones sobre tus endpoints y generará una especificación OpenAPI.
//npm i swagger-jsdoc
//swagger-ui-express: este módulo te proveerá de una web para poder visualizar la documentación.
//npm i swagger-ui-express 

//agregos los paquetes
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
//opciones generales de tu proyecto
const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: 'prueba acamica swagger',
        version: '1.0.0'
      }
    },
    apis: ['./server.js'],
  };
//creacion de la instancia swagger
const swaggerDocs = swaggerJsDoc(swaggerOptions);
//endpoint que accede a la documentacion
server.use('/api-docs',swaggerUI.serve,swaggerUI.setup(swaggerDocs));//localhost:3000/api-docs
//PARA QUE DOCUMENTE SE AGREGA LO SIGUIENTE ANTES DEL ENDPOINT

/**
 * @swagger
 * /profesores:
 *  post:
 *  description: Crea un nuevo profesor
 *
 */
 server.post('/profesores', (req, res) => {
    res.status(201).send();
  });

  /**
    Parameter: indica el comienzo de la descripción de parámetros.
    Name: se utiliza para colocar el nombre del parámetro.
    Description: aquí irá la descripción del parámetro.
    In: aquí irá el parámetro.
    Required: indica si el parámetro es requerido o no (true/false).
    Type: es el tipo de dato del parámetro. Los más usados son string / integer.
    Responses: mostrará qué tipo de respuesta tiene y su descripción.
   */

    /**
 * @swagger
 * /estudiantes:
 *  post:
 *    description: Crea un nuevo estudiante
 *    parameters:
 *    - name: nombre
 *      description: Nombre del estudiante
 *      in: formData
 *      required: true
 *      type: string
 *    - name: edad
 *      description: Edad del estudiante
 *      in: formData
 *      required: true
 *      type: integer
 *    responses:
 *      200:
 *        description: Success
 * 
 */
server.post('/estudiantes', (req, res) => {
    res.status(201).send();
  });


server.listen(puerto,()=>console.log("escuchando en puerto 3000"));
