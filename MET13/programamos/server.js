const env = require("./enviroment.json");
const puerto = env.development.PORT;

const express = require("express");
const server = express();
server.use(express.urlencoded({extended:true}));
server.use(express.json())

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: 'vehiculos swagger',
        version: '1.0.0'
      }
    },
    apis: ['./swager.js'],
}

const swaggerDocs = swaggerJsDoc(swaggerOptions);

server.use('/api-docs',swaggerUI.serve,swaggerUI.setup(swaggerDocs));//localhost:3000/api-docs

//en grupo

var usuarios = [{
    id:1,
    nom:"mariano",
    ape:"perez",
    email:"mariano@email.com"
}]

var topicos = [{
    id:1,
    titulo:"titulo",
    descripcion:"descripcion"
}]

var comentarios = [{
    id:1,
    topico_id:1,
    usuario_id:1,
    comentario:"comentario"
}]


server.get("/usuarios",(req,res)=>{
    res.status(201).send(usuarios);
})

 server.post("/usuarios",(req,res)=>{
     const{id,nom,ape,email} = req.body;
     if(!req.body.id ||!req.body.nom || !req.body.ape ||!req.body.email){
         res.status(404).send("Debe ingresar todos los campos");
     }else{
         if(req.body.id !="" ||req.body.nom !="" || req.body.ape!="" ||req.body.email!=""){
            const usuario = {
                id:parseInt(id),
                nom:nom,
                ape:ape,
                email:email
            }
            usuarios.push(usuario);
            res.status(201).send("Agregado de usuario");
        }
    }
});

//USUARIOS--------------------------------------------------------------

/**
 * @swagger
 * /usuarios:
 *  put:
 *   description: Actualiza un usuario
 *   parameters:
 *   - name: id
 *     type: integer
 *     description: Id del usuario
 *     in: formData
 *     required: true
 *   - name: nom
 *     type: string
 *     description: Nombre del usuario
 *     in: formData
 *     required: true
 *   - name: ape
 *     type: string
 *     description: Apellido del usuario
 *     in: formData
 *     required: true
 *   - name: email
 *     type: string
 *     description: Email del usuario
 *     in: formData
 *     required: true
 *   responses:
 *     200:
 *       description: Sucess
 *
 */
 server.put("/usuarios",(req,res)=>{
    const{id,nom,ape,email} = req.body;
    if(!req.body.id ||!req.body.nom || !req.body.ape ||!req.body.email){
        res.status(404).send("Debe ingresar todos los campos");
    }else{
        if(req.body.id !=="" ||req.body.nom !=="" || req.body.ape!=="" ||req.body.email!==""){
            usuarios.map((item)=>{
                if(item.id === parseInt(id)){
                    item.id = parseInt(id);
                    item.nom = nom;
                    item.ape = ape;
                    item.email = email;
                    
                }
            });
            res.status(201).send(`Usuario actualizado ${usuarios}`)
        }
    }
})


/**
 * @swagger
 * /usuarios:
 *  delete:
 *   description: Eliminar un usuario
 *   parameters:
 *   - name: id
 *     type: integer
 *     description: id de usuario a eliminar
 *     in: formData
 *     required: true
 *   responses:
 *     200:
 *       description: Sucess
 *
 */
 server.delete("/usuarios",(req,res)=>{
    const {id} = req.body;
    if(!req.body.id){
         res.status(404).send("Debe ingresar el campo id");
    }else{
        if(req.body.id !==""){
            usuarios = usuarios.filter(item => item.id !== parseInt(req.body.id))
            res.status(201).send("Eliminacion de usuario");
        }
    }
});

//TOPICOS--------------------------------------------------------

/**
 * @swagger
 * /topicos:
 *  get:
 *   description: Obtener listado de topicos
 *   responses:
 *     200:
 *       descrption: Sucess
 */
server.get("/topicos",(req,res)=>{
    res.status(201).send(topicos);
});

/**
 * @swagger
 * /topicos:
 *  post:
 *   description: Registro de topicos
 *   parameters:
 *   - name: id
 *     type: integer
 *     required: true
 *     in : formData
 *     description: id del topico
 *   - name: titulo
 *     type: string
 *     in: formData
 *     required: true
 *     description: titulo del topico
 *   - name: descripcion
 *     type: string
 *     in: formData
 *     required: true
 *     description: descripcion del topico
 *   responses:
 *     200:
 *       description: Sucess
 */
server.post("/topicos",(req,res)=>{
    const{id,titulo,descripcion}=req.body;
    if(!req.body.id || !req.body.titulo || !req.body.descripcion){
        res.status(404).send("SE REQUIERE RELLENAR TODOS LOS CAMPOS")
    }else{
        if(req.body.id !=="" || req.body.titulo!=="" || req.body.descripcion!==""){
            const datos = {
                id:parseInt(id),
                titulo:titulo,
                descripcion:descripcion,
            }
            topicos.push(datos);
            res.status(201).send("TOPICO EXITOSAMENTE AGREGADO")
        }
    }
})

/**
 * @swagger
 * /topicos:
 *  delete:
 *   description: Elimina un topico
 *   parameters:
 *   - name: id
 *     type : integer
 *     in: formData
 *     required: true
 *     description: Id del topico
 *   responses:
 *     200:
 *       Sucess
 */
server.delete("/topicos",(req,res)=>{
    if(!req.body.id){
        res.status(404).send("SE REQUIERE EL ID");
    }else{
        if(req.body.id!==""){
            topicos = topicos.filter(item => item.id !== parseInt(req.body.id))
            res.status(201).send("TOPICO ELIMINADO EXITOSAMENTE");
        }
    }
})

/**
 * @swagger
 * /topicos:
 *  put:
 *   description: Actualizacion de topico
 *   parameters:
 *   - name: id
 *     type: integer
 *     in: formData
 *     required: true
 *     description: Id del topico
 *   - name: titulo
 *     type: string
 *     in: formData
 *     required: true
 *     description: Titulo del topico
 *   - name: descripcion
 *     type: string
 *     in: formData
 *     required: true
 *     description: descripcion del topico
 *   responses:
 *     200:
 *       Sucess
 */
server.put("/topicos",(req,res)=>{
    const{id,titulo,descripcion} = req.body;
    if(!req.body.id || !req.body.titulo || !req.body.descripcion){
        res.status(404).send("SE REQUIEREN TODOS LOS CAMPO")
    }else{
        if(req.body.id!=="" || req.body.titulo!=="" || req.body.descripcion!==""){
            topicos.map((item)=>{
                if(item.id === parseInt(id)){
                    item.id = parseInt(id);
                    item.titulo = titulo;
                    item.descripcion = descripcion;
                }
            });
            res.send(201).send(`TOPICO ACTUALIZADO EXITOSAMENTE`)
        }
    }
})

//COMENTARIOS------------------------------------------------------

/**
 * @swagger
 * /comentarios:
 *  post:
 *   description: Registrar comentario
 *   parameters: 
 *   - name: id
 *     type: integer
 *     in: formData
 *     required: true
 *     description: id de comentario
 *   - name: topico_id
 *     type: integer
 *     in: formData
 *     required: true
 *     description: id topico al que pertenece el comentario
 *   - name: usuario_id
 *     type: integer
 *     in: formData
 *     required: true
 *     description: id del usuario al que pertenece el comentario
 *   - name: comentario
 *     type: string
 *     in: formData
 *     required: true
 *     description: comentario
 *   responses:
 *     200:
 *       Sucess
 */
server.post("/comentarios",(req,res)=>{
    const{id,topico_id,usuario_id,comentario}=req.body;
    if(!req.body.id || !req.body.topico_id || !req.body.usuario_id || !req.body.comentario){
        res.status(404).send("SE REQUIEREN TODOS LOS CAMPOS")
    }else{
        if(req.body.id !=="" || req.body.topico_id !=="" || req.body.usuario_id !=="" || req.body.comentario !==""){
            const datos = {
                id : parseInt(id),
                topico_id : topico_id,
                usuario_id : usuario_id,
                comentario : comentario,
            }
            comentarios.push(datos);
            res.status(202).send("COMENTARIO REGISTRADO")
        }
    }
})

/**
 * @swagger
 * /comentarios:
 *  get:
 *   description: Obtener los comentarios
 *   responses:
 *     200:
 *       Sucess
 */
server.get("/comentarios",(req,res)=>{
    res.status(202).send(comentarios);
})

/**
 * @swagger
 * /comentarios:
 *  put:
 *   description: Actualizar un comentario
 *   parameters: 
 *   - name: id
 *     type: integer
 *     in: formData
 *     required: true
 *     description: id de comentario
 *   - name: topico_id
 *     type: integer
 *     in: formData
 *     required: true
 *     description: id topico al que pertenece el comentario
 *   - name: usuario_id
 *     type: integer
 *     in: formData
 *     required: true
 *     description: id del usuario al que pertenece el comentario
 *   - name: comentario
 *     type: string
 *     in: formData
 *     required: true
 *     description: comentario
 *   responses:
 *     200:
 *       Sucess
 */
 server.put("/comentarios",(req,res)=>{
    const{id,topico_id,usuario_id,comentario}=req.body;
    if(!req.body.id || !req.body.topico_id || !req.body.usuario_id || !req.body.comentario){
        res.status(404).send("SE REQUIEREN TODOS LOS CAMPOS")
    }else{
        if(req.body.id !=="" || req.body.topico_id !=="" || req.body.usuario_id !=="" || req.body.comentario !==""){
            comentarios.map((item)=>{
                if(item.id=== parseInt(req.body.id)){
                    item.id = parseInt(id);
                    item.topico_id = topico_id;
                    item.usuario_id = usuario_id;
                    item.comentario = comentario;
                }
            })
            res.status(202).send("COMENTARIO ACTUALIZADO")
        }
    }
})


/**
 * @swagger
 * /comentarios:
 *  delete:
 *   description: Eliminar un comentario
 *   parameters: 
 *   - name: id
 *     type: integer
 *     in: formData
 *     required: true
 *     description: id de comentario
 *   responses:
 *     200:
 *       Sucess
 */
 server.delete("/comentarios",(req,res)=>{
    const{id}=req.body;
    if(!req.body.id ){
        res.status(404).send("SE REQUIEREN TODOS LOS CAMPOS")
    }else{
        if(req.body.id !=="" ){
            comentarios = comentarios.filter(item => item.id !== parseInt(req.body.id))
            res.status(202).send("COMENTARIO ELIMINADO")
        }
    }
})
server.listen(puerto,()=>console.log("escuchando en puerto "+puerto));

