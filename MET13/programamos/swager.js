/**
 *  @swagger
 * /usuarios:
 *  get:
 *   description: Obtiene los usuarios
 *   responses:
 *     200:
 *       description: Sucess
 * 
 */

/**
 * @swagger
 * /usuarios:
 *  put:
 *   description: Actualiza un usuario
 *   parameters:
 *   - name: id
 *     type: integer
 *     description: Id del usuario
 *     in: formData
 *     required: true
 *   - name: nom
 *     type: string
 *     description: Nombre del usuario
 *     in: formData
 *     required: true
 *   - name: ape
 *     type: string
 *     description: Apellido del usuario
 *     in: formData
 *     required: true
 *   - name: email
 *     type: string
 *     description: Email del usuario
 *     in: formData
 *     required: true
 *   responses:
 *     200:
 *       description: Sucess
 *
 */