const env = require("./enviroment.json");
const puerto = env.development.PORT;

const express = require("express");
const server = express();
server.use(express.urlencoded({extended:true}));
server.use(express.json())

const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const swaggerOptions = {
    swaggerDefinition: {
      info: {
        title: 'swagger clase 14',
        version: '1.0.0'
      }
    },
    apis: ['./server.js'],
}

const swaggerDocs = swaggerJsDoc(swaggerOptions);

server.use('/api-docs',swaggerUI.serve,swaggerUI.setup(swaggerDocs));//localhost:3000/api-docs

var resultado =  false;

var usuarios = [
    {
        user:"administrador",
        pass:"123",
        admin:true
    }
   /** {
        id:Number,
        user:String,
        nom_ape:String,
        email:String,
        tel:Number,
        direc:String,
        pass:String,
        admin:Boolean
    }*/
]

var productos = [
    {
        id:Number,
        stock:Number,
        nom:String,
    }
]

var pedidos = [
    {
        id:Number,
        productos:[
            {
                id:Number,
                nom:String
            }
        ],
        cantidad:Number,
        id_usuario:Number,
    }
]

//middleware
function validarProducto(req,res,next){
    if(!req.query.id || !req.query.stock || !req.query.nom ){
        res.send("se requieren todos los campos");
    }else{
        if(req.query.id !=="" || req.query.stock !=="" || req.query.nom !==""){
         next();
        }else{
          res.send("se requieren que los campos no esten vacios");
        }
    }
};

function validarusuario(req,res,next){
    if(!req.query.id || !req.query.user || !req.query.nom_ape || !req.query.email || !req.query.telefono || !req.query.direccion || !req.query.pass){
        res.status(404).send("SE REQUIEREN TODOS LOS CAMPOS");
    }else{
        if(req.query.id !=="" || req.query.user !=="" || req.query.nom_ape !=="" || req.query.email !=="" || req.query.telefono !=="" || req.query.direccion !=="" || req.query.pass !=="" ){
            next();
        }else{
          res.send("se requieren que los campos no esten vacios");
        }
    }          
};

function validarLogin(req,res,next){
    if(!req.query.user || !req.query.pass){
        res.status(404).send("SE REQUIEREN TODOS LO CAMPOS")
    }else{
        if(req.query.user !=="" || req.query.pass !==""){
            next()
        }else{
            res.send("se requieren que los campos no esten vacios");
        }
    }  
}

function validarlogeo(req,res,next){
    if(this.resultado==true){
        next();
    }else{
        res.status(404).send("usted no esta logeado")
    }
}

//endpoint
/**
 * @swagger
 * /usuarios:
 *  post:
 *    descriprion: Creacion de usuario
 *    parameters: 
 *    - name: id
 *      type: integer
 *      in: formData
 *      required: true
 *      description : id del usuario
 *    - name: user
 *      type: string
 *      in: formData
 *      required: true
 *      description : nombre de usuario
 *    - name: nom_ape
 *      type: string
 *      in: formData
 *      required: true
 *      description : nombre y apellido del usuarios
 *    - name: email
 *      type: string
 *      in: formData
 *      required: true
 *      description: correo electronico del usuario
 *    - name: telefono
 *      type: integer
 *      in: formData
 *      required: true
 *      description: telefono del usuario
 *    - name: direccion
 *      type: string
 *      in: formData
 *      required: true
 *      description: direccion del usuario
 *    - name: pass
 *      type: string
 *      in: formData
 *      required: true
 *      description: contraseña del usuario
 *    responses:
 *      200:
 *        Sucess
 */

server.post("/usuarios",validarusuario,(req,res)=>{
    const datos = {
        id:parseInt(req.body.id),
        user:req.body.user,
        nom_ape:req.body.nom_ape,
        email:req.body.email,
        tel:req.body.telefono,
        direc:req.body.direccion,
        pass:req.body.pass,
        admin:false
    }
    usuarios.push(datos);
    res.status(201).send("SE CREO EXITOSAMENTE EL NUEVO USUARIO")
})

/**
 * @swagger
 * /login:
 *  post:
 *    descriprion: Verificar existencia del usuario
 *    parameters: 
 *    - name: user
 *      type: string
 *      in: formData
 *      required: true
 *      description : usuario
 *    - name: pass
 *      type: string
 *      in: formData
 *      required: true
 *      description : contraseña del usuario
 *    responses:
 *      200:
 *        Sucess
 */
server.post("/login",validarLogin,(req,res)=>{
    let resp = usuarios.filter(item => item.user === req.body.user && item.pass === req.body.pass)
    if(resp != undefined){
        this.resultado=true;
        res.status(202).send(resp)
    }else{
    res.status(404).send("El usuario no se encontro")
    }
})


/**
 * @swagger
 * /productos:
 *  get:
 *    descriprion: Obtener usuarios
 *    parameters:
 *    - name: user
 *      type: string
 *      in: formData
 *      required: true
 *      description: Nombre de usuario
 *    responses:
 *      200:
 *        Sucess
 */
server.get("/productos",validarlogeo,(req,res)=>{
        res.status(201).send(productos);
})


/**
 * @swagger
 * /productos:
 *  post:
 *   description: Crear producto
 *   parameters:
 *   - name: id
 *     type: integer
 *     in: fornData
 *     requiered: true
 *     description: id del producto
 *   - name: stock
 *     type: integer
 *     in: fornData
 *     requiered: true
 *     description: cantidad de stock del producto
 *   - name: nom
 *     type: string
 *     in: fornData
 *     requiered: true
 *     description: nombre del producto
 *   responses:
 *     200: 
 *       sucess
 * 
 */
server.post("/productos",validarProducto,(req,res)=>{
    const datos = {
        id : parseInt(req.body.id),
        stock : req.body.stock,
        nom: req.body.nom
    }
    productos.push(datos);
    res.status(201).send("Producto agregado")
})

/**
 * @swagger
 * /productos:
 *  put:
 *   description: Modificar un producto
 *   parameters:
 *   - name: id
 *     type: integer
 *     in: fornData
 *     requiered: true
 *     description: id del producto
 *   - name: stock
 *     type: integer
 *     in: fornData
 *     requiered: true
 *     description: cantidad de stock del producto
 *   - name: nom
 *     type: string
 *     in: fornData
 *     requiered: true
 *     description: nombre del producto
 *   responses:
 *     200: 
 *       sucess
 * 
 */
 server.put("/productos",validarProducto,(req,res)=>{
    productos.map((item)=>{
        if(item.id === parseInt(req.body.id)){
            item.id = parseInt(req.body.id);
            item.stock = req.body.stock;
            item.nom = req.body.nom
        }
    })
    res.status(201).send("Producto modificado")
})


server.listen(puerto,()=>{console.log(`eschando en ${env.development.SERVERURL}`)})