

//primera parte
const unAuto = {
    color:"rojo",
    cantuertas:5,
mostrarMensaje :function(){
    return `Mi auto es de color : ${this.color}`;
}
}

const otroAuto = {
color:"verde",
cantuertas:4,
mostrarMensaje : function(){
    return `Mi auto es de color : ${this.color}`;
}
}

document.getElementById("auto1").innerHTML = unAuto.mostrarMensaje();
document.getElementById("auto1").style.color = "red";
document.getElementById("auto2").innerHTML = otroAuto.mostrarMensaje();
document.getElementById("auto2").style.color = "green";


class AutoMovil{
constructor(color,marca,cantpuertas){
    this.color = color;
    this.marca = marca;
    this.cantpuertas = cantpuertas;
}
mostrarColor(){
    return `---El color es:${this.color}`;
}
mostrarMarca(){
    return `--La marca es:${this.marca}`;
}
mostrarCantPuertas(){
    return `--La cantidad de puertas es:${this.cantpuertas}`;
}
}

let auto_toy = new AutoMovil("rojo","Toyota",3);
let auto_mer = new AutoMovil("morado","Mercedes",2);
let auto_bmw = new AutoMovil("naranja","BMW",4);

console.log(auto_toy.mostrarColor()+auto_toy.mostrarMarca()+auto_toy.mostrarCantPuertas());
console.log(auto_mer.mostrarColor()+auto_mer.mostrarMarca()+auto_mer.mostrarCantPuertas());
console.log(auto_bmw.mostrarColor()+auto_bmw.mostrarMarca()+auto_bmw.mostrarCantPuertas());

class User{
constructor(nom,ape){
    this.nom = nom;
    this.ape = ape;
}

get nombre(){
    return this.nom;
}
set nombre(valor){
    this.nom = valor;
}
get apellido(){
    return this.ape;
}
set apellido(valor){
    this.ape = valor;
}
get nomape(){
    return this.nom +" "+ this.ape;
}
MostrarAñodeProduccion(){
    return 1999;
}
static Info_clase(){
    return "aca va la informacion de la clase";
}
}

let persona = new User("Juancito","Perez");
console.log("Nombre y apellido:"+persona.nomape);
persona.nombre="ANA";
persona.apellido="GARCIA";
console.log("Nombre y apellido:"+persona.nomape);

console.log("AÑO DE PRODUCCION:"+persona.MostrarAñodeProduccion());

console.log("METODO ESTATICO:"+User.Info_clase());

class Animal{
constructor(nom){
    this.nom = nom;
}
hablar(){
    console.log(this.nom+" hace ruido");
}
}

class Perro extends Animal{
hablar(){
    console.log(this.nom+" WOF WOF WOF");
}
}

class Gato extends Animal{
hablar(){
    console.log(this.nom+" MIAU MIAU MIAU");
}
}

let perro1 = new Perro("FIRULAIS");
perro1.hablar();

let perro2 = new Perro("PERRO GUATON");
perro2.hablar();

let gato1 = new Gato("MICHUNGO");
gato1.hablar();


//segunda parte
class Persona{
    constructor(nom,ape,edad){
        this.nom = nom;
        this.ape = ape;
        this.edad = edad;
    }
    fullname(){
        return this.nom +" "+ this.ape;
    }
    mayor(){
        if(this.edad>18){
            return true;
        }else{
            return false;
        }
    }
}

var juanita = new Persona("Juanita","MACARRONE",20);
document.getElementById("juanita").innerHTML = "SU NOMBRE Y APELLIDO ES :" +juanita.fullname() + "---ES MAYOR?:" + juanita.mayor();

var juanito = new Persona("JUANITO","GOMEZ",11);
document.getElementById("juanito").innerHTML = "SU NOMBRE Y APELLIDO ES :" +juanito.fullname() + "---ES MAYOR?:" + juanito.mayor();

//tercera parte class Perros{
    constructor(estado,nom,raza,edad,color,peso){
        this.estado = estado;
        this.nom = nom;
        this.raza = raza;
        this.edad = edad;
        this.color = color;
        this.peso = peso;
    }

    set cambiarest(valor){
        this.estado = valor;
    }

    informar(){
        return this.estado;
    }
}

var perros = [];

do{
    var nom = prompt("Ingrese el nombre");
    var estado = prompt("Ingrese el estado");
    var raza = prompt("Ingrese la raza");
    var edad = prompt("Ingrese la edad");
    var color= prompt("Ingrese el color");
    var peso= prompt("Ingrese el peso");
    
    const perro = new Perros(estado,nom,raza,edad,color,peso);
    perros.push(perro);

    console.log(perro.informar());

}while(window.confirm("Desea agaregar mas?"));

var perros_adoptado = [];
var perros_en_proceso = [];
var perros_adopcion = [];

for(var i = 0;i<perros.length;i++){
    if(perros[i].estado=="en adopcion")perros_adopcion.push(perros[i]);
    if(perros[i].estado=="adoptado")perros_adoptado.push(perros[i]);
    if(perros[i].estado=="proceso de adopcion")perros_en_proceso.push(perros[i]);
}

console.log("PERROS EN ADOPCION-------------");
for(var i = 0;i < perros_adopcion.length;i++){
    console.log(perros_adopcion[i]);
}

console.log("PERROS EN PROCESO DE ADOPCION-------------");
for(var i = 0;i < perros_en_proceso.length;i++){
    console.log(perros_en_proceso[i]);
}

console.log("PERROS ADOPTADPS-------------");
for(var i = 0;i < perros_adoptado.length;i++){
    console.log(perros_adoptado[i]);
}
