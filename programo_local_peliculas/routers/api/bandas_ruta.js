const router = require("express").Router();
const {Bandas} = require("../../db")
const {Albums} = require("../..")

router.get("/bandas",async(req,res)=>{
    const bandas = await Bandas.findAll()
    res.send(bandas)
})

//traer todos los albumes de un banda

router.get("/bandas/:id",async(req,res)=>{
    await Bandas.findAll()
})

router.post("/crear",async(req,res)=>{
    const bandas = await Bandas.create(req.body)
    res.send("SE HA CREADO EXITOSAMENTE")
})

router.delete("/eliminar/:id",async(req,res)=>{
    await Bandas.destroy({
        where:{
            id:req.params.id
        }
    }).then(function(deletedRecord){
        if(deletedRecord === 1){
            res.status(202).send("SE HA ELIMINADO EXITOSAMENTE")
        }else{
            res.status(404).send("HA OCURRIDO UN ERROR EN EL SISTEMA")
        }
    }).catch(function(err){
        res.status(500).send("NO SE HA BORRADO")
    })
})


router.put("/modificar/:id",async(req,res)=>{
    await Bandas.update(req.body,{
        where:{
            id:req.params.id
        }
    })
    res.status(202).send("SE MODIFCO EXITOSAMENTE")
})


module.exports = router;