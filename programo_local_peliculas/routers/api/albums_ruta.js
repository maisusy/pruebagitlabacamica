const router = require("express").Router();
const {Albums} = require("../../db")

router.get("/",async(req,res)=>{
    const albums = await Albums.findAll()
    res.send(albums)
})

router.post("/crear",async(req,res)=>{
    await Albums.create(req.body)
    res.send("SE HA REGISTRADO EXITOSAMENTE")
})

router.put("/modificar/:id",async(req,res)=>{
    await Albums.update(req.body,{
        where:{
            id:req.params.id
        }
    })
    res.send("SE HA ACTUALIZADO EXITOSAMENTE")
})

router.delete("/eliminar/:id",async(req,res)=>{
    await Albums.destroy({
        where:{
            id:req.params.id
        }
    }).then(function(deletedRecord){
        if(deletedRecord === 1){
            res.status(202).send("SE ELIMINO EXITOSAMENTE")
        }else{
            res.status(404).send("HA OCURRIDO UN ERROR")
        }
    }).catch(function(err){
        console.log(err)
        res.status(500).send("NO SE ELIMINO ")
    })
})

module.exports = router;