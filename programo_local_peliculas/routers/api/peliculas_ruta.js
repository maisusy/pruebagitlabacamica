const router = require("express").Router()
const {Pelicula} = require("../../db")


router.post("/crear",async(req,res)=>{
    const pelicula = await Pelicula.create(req.body)
    res.send("creado")
});

router.put("/modificar/:id",async(req,res)=>{
    await Pelicula.update(req.body,{
    where:{id:req.params.id}
    })
    res.send("modifcado")
});

router.delete("/eliminar/:id",async(req,res)=>{
    await Pelicula.destroy({
        where:{
            id:req.params.id
        }
    })
    .then(function(deletedRecord){
        if(deletedRecord === 1){
            res.status(200).send("borro ok")
        }else{

            res.status(404).send("no se borro ")
        }
    }).catch(function(err){
        res.status(500).send("no se borro ")
    })
});

//listado peliculas
router.get("/",async(req,res)=>{
    const pelicula = await Pelicula.findAll()
    res.send(pelicula);
});


module.exports = router;