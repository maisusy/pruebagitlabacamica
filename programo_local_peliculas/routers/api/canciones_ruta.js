const router = require("express").Router()
const {Canciones} = require("../../db")

router.get("/",async(req,res)=>{
    const canciones = await Canciones.findAll();
    res.send(canciones);
})

router.post("/crear",async(req,res)=>{
    await Canciones.create(req.body);
    res.send("SE HA CREADO EXITOSAMENTE")
})

router.delete("/eliminar/:id",async(req,res)=>{
    await Canciones.destroy({
        where:{
            id:req.params.id
        }
    }).then(function(deletedRecord) {
        if(deletedRecord === 1){
            res.status(202).send("SE HA ELIMINADO EXITOSAMENTE")
        }else{
            res.status(404).send("HA OCURRIDO UN ERROR EN EL SERVIDOR")
        }
    }).catch(function(err) {
        res.status(500).send("NO SE HA ELIMINADO")
    })
})

router.put("/modificar/:id",async(req,res)=>{
    await Canciones.update(req.body,{
        where:{
            id:req.params.id
        }
    })
    res.send("SE HA ACTUALIZADO EXITOSAMENTE")
})

module.exports = router;