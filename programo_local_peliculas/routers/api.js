const router = require("express").Router()

const apiPelicula  = require("./api/peliculas_ruta")
const apiBanda = require("./api/bandas_ruta")
const apiCancion = require("./api/canciones_ruta")
const apiAlbum = require("./api/albums_ruta")

router.use("/peliculas",apiPelicula)
router.use("/banda",apiBanda)
router.use("/cancion",apiCancion)
router.use("/album",apiAlbum)


module.exports = router;