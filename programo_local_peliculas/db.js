const Sequelize = require("sequelize")
const PeliculaModelo = require("./models/pelicula")
const BandasModelo  = require("./models/bandas")
const CancionesModelo = require("./models/canciones")
const AlbumsModelo = require("./models/albums")

const sequelize = new Sequelize('acamica','root','',{
    host:"localhost",
    dialect:"mysql"
})


validar_conexion();

const Pelicula = PeliculaModelo(sequelize,Sequelize)

const Bandas = BandasModelo(sequelize,Sequelize)

const Canciones =  CancionesModelo(sequelize,Sequelize)

const Albums = AlbumsModelo(sequelize,Sequelize)

sequelize.sync({force:false})
    .then(()=>{
        console.log("tablas sincronizadas")
});


async function validar_conexion(){
    try{
        await sequelize.authenticate();
        console.log("conexion ha sido establecida")
    }catch(err){
        console.log(err)
    }
}

module.exports = {
    Pelicula,
    Bandas,
    Canciones,
    Albums
}