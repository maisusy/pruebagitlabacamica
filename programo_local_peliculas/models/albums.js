module.exports = (sequelize,type)=>{
    return sequelize.define("Albums",
    {
        id:{
            type: type.INTEGER,
            primaryKey : true,
            autoIncrement : true
        },
        fecha_publicacion: type.DATE,
        nombre: type.STRING,
        banda_id: {
            type: type.INTEGER,
            references:{
                model: "Bandas",
                key : "id"
            }
        }
    });
    
}