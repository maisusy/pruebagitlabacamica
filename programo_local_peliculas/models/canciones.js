module.exports = (sequelize,type)=>{
    return sequelize.define("Canciones",
    {
        nombre : type.STRING,
        id:{
            type : type.INTEGER,
            primaryKey : true,
            autoIncrement : true
        },
        banda_id : {
            type : type.INTEGER,
            references : {
                model : "Bandas",
                key : "id"
            }
        },
        album_id : {
            type : type.INTEGER,
            references : {
                model : "Albums",
                key : "id"
            }
        },
        duracion : type.TIME,
        fecha_publicacion : type.DATE
    });
}