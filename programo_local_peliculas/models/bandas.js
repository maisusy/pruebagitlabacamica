module.exports = (sequelize,type)=>{
    return sequelize.define("Bandas",
    {
        id:{
            type : type.INTEGER,
            primaryKey : true,
            autoIncrement : true
        },
        nombre : type.STRING,
        pais : type.STRING,
        fecha_inicio : type.DATE,
        fecha_separacion : type.DATE,
        integrantes_num : type.INTEGER
    });
}