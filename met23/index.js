const {Sequelize,Model,DataTypes}= require("sequelize")
const express = require("express")
const app = express()
app.use(express.json())
app.use(express.urlencoded({extended:true}));


app.listen(2020,()=>{
    console.log("listen 2020")
})


const sequelize = new Sequelize(
    'venta_tele','root','',{
        host:'localhost',
        dialect:'mysql'
    }
)


sequelize
    .authenticate()
    .then(()=>{
        console.log("conexion oki")
    })
    .catch(()=>{
        console.log("conexion bad")
    })

class Marcas extends Model{}

Marcas.init(
    {
        nombre: DataTypes.STRING
    },
    {
        sequelize,
        modelName:"marcas",
        timestamps:false
    }
)



class Modelos extends Model{}

Modelos.init(
    {
        nombre: DataTypes.STRING
    },
    {
        sequelize,
        modelName:"modelos",
        timestamps:false
    }
)



class TV extends Model {}

TV.init(
    {
        pantalla : DataTypes.INTEGER,
        smart : DataTypes.BOOLEAN,
        precio : DataTypes.INTEGER
    },
    {
        sequelize,
        modelName:"TV",
        timestamps:false
    }
)

Modelos.belongsTo(Marcas,{foreignKey:"id_marca"}); 
TV.belongsTo(Marcas,{foreignKey:"id_marca"}); 
TV.belongsTo(Modelos,{foreignKey:"id_modelo"});


//listar segun precio mayor o menor

app.get("/lista",async(req,res)=>{
    const datos = await TV.findAll()
    if(datos === null){
        console.log("no se encontro ninguna tele")
    }else{
        res.json("teles",datos)
    }
})

app.post("/crear",async(req,res)=>{
    await TV.create(req.body)
    res.json("tv creada")
})

//lista s marca
app.get("/lista/:marca",async(req,res)=>{
    const datos = await TV.findAll({
        where:{
            marca:req.params.marca
        }
    })
    if(datos === null){
        console.log("no se encontro ninguna tele")
    }else{
        res.json("teles",datos)
    }
})

//mayores de x precio
app.get("/lista/:precio",async(req,res)=>{
    const datos = await TV.findAll({
        where:{
            precio : {
                [Op.gt]: parseInt(req.params.precio)
            }
        }
    })

    if(datos === null){
        console.log("no se encontro ninguna tele")
    }else{
        res.json("teles",datos)
    }
})

//menores de x precio
app.get("/lista/:precio",async(req,res)=>{
    const datos = await TV.findAll({
        where:{
            precio : {
                [Op.lt]: parseInt(req.params.precio)
            }
        }
    })

    if(datos === null){
        console.log("no se encontro ninguna tele")
    }else{
        res.json("teles",datos)
    }
})

//ordenas menor a mayor s precio
app.get("lista_ord_decr",async(req,res)=>{
    const datos = await TV.findAll({
        order:[
            ["precio","DESC"]
        ]
        
    })


    if(datos === null){
        console.log("no se encontro ninguna tele")
    }else{
        res.json("teles",datos)
    }
})


/** 
class Usuarios extends Model {}

Usuarios.init(
    {
        nombre: DataTypes.STRING,
        apellidos: DataTypes.STRING
    },
    {
        sequelize,
        modelName:"usuarios",
        timestamps:false //quita el createad y updatead
    }
)

class Casas extends Model{}

Casas.init(
    {
        nombre: DataTypes.STRING
    },
    {
        sequelize,
        modelName:"casas",
        timestamps:false
    }
)
//asociacion usando belongsto
//si la llave foranea la va a tener la primera tabla usamos belong
Usuarios.belongsTo(Casas,{foreignKey:"id_casa"});
//si la llave foranea la va a tener la segunda tabla se usa hasone
//Usuarios.hasOne(Casas)

//funcion autoinvocada para realizar las operaciones

(async()=>{

    await sequelize.sync({force:true})

    const datoPlaya = {nombre:"casa de la playa"}
    const dataInsertCasa = await Casas.create(datoPlaya)
    console.log(dataInsertCasa.toJSON())

    const datos = {nombre:"josefa",apellidos:"carolina",id_casa:1}
    const usuarioData = await Usuarios.create(datos)
    console.log(usuarioData.toJSON())

    console.log(usuarioData.nombre)
    console.log(usuarioData.apellidos)


    usuarioData.nombre = "manuela"
    usuarioData.apellidos = "jimenez update"

    await usuarioData.save()

    console.log(usuarioData.nombre)
    console.log(usuarioData.apellidos)

    //eliminamos
    //await usuarioData.destroy()

    //busca un solo resultado
    const userOne = await Usuarios.findOne({
        where:{
            nombre:"manuela"
        }
    })

    if(userOne === null){
        console.log("ususario no encontrado")
    }else{
        console.log("usuario encontrado")
    }

    const userMany  = await Usuarios.findAll({
        where:{
            nombre:"manuela"
        }
    })

    userMany.forEach(element => {
        console.log(element.toJSON())
    });

    let usuariosPlaya = await Usuarios.findAll({
        include:{
            model:Casas
        },
        attributes:["nombre","apellidos"]
    })

    usuariosPlaya.forEach((item)=>{
        console.log("datos",item.toJSON())
    })
})()
*/
