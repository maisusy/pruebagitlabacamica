const router = require("express").Router()

const apiFrutas = require("./api/fruta")
//const apiProducto = require("./api/producto")
//const apiUsuario = require("./api/usuario")

router.use("/frutas",apiFrutas)

module.exports = router
