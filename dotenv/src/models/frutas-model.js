module.exports = (sequelize,type) =>{
    return sequelize.define("Fruta",{
        nombre : type.STRING,
        color : type.STRING,
        vitaminas : type.STRING
    });
}