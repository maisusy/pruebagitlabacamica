//npm i paquete

// npm i moment
var moment = require('moment');

//fecha actual
var now = moment(new Date());

//fecha utc
var utc = moment(new Date()).utc();

console.log('actual',now);
console.log('utc',utc);
var diferencia = now.hours() - utc.hours();
console.log('diferencia',diferencia);

// //Muestro ambas fechas
console.log(now.format('DD MM YYYY'));
console.log(utc.format('hh:mm:ss'));

var duration = new moment(utc.diff(now));

var primera_fecha = moment([2021, 07, 07]);
var segunda_fecha = moment([2021, 07, 12]);

// Diferencias en dias
var result_dias_diff = primera_fecha.diff(segunda_fecha, 'days');
console.log("Nro dias diff:", result_dias_diff);

// Diferncias en horas
var result_horas_diff = primera_fecha.diff(segunda_fecha, 'hours');
console.log("Nro horas diff:", result_horas_diff);

//valor absoluto
var duration = moment.duration(primera_fecha.diff(segunda_fecha));
var days = Math.abs((duration.asDays()));
console.log(days);

//Identificar que fecha es mayor o menor utilizando el metodo isBefore
var fecha1 = '1980-06-10';
var fecha2 = '2021-07-07';

// ¿la fecha1 esta antes de la fecha 2 ? >>> True
console.log(moment(fecha1).isBefore(fecha2));
if(moment(fecha1).isBefore(fecha2)){
    console.log('La fecha2 '+fecha2+' es mayor que la fecha1 '+fecha1);  
}else{
    console.log('La fecha1 '+fecha1+' es mayor que la fecha2 '+fecha2);  
}



//npm i chalk
const chalk = require('chalk');

const log = console.log;

// Combine styled and normal strings
log(chalk.green('HOLA') + ' MUNDO' + chalk.red('!'));

// Compose multiple styles using the chainable API
log(chalk.white.bgRed.bold('HOLA MUNDO!'));

// Pass in multiple arguments
log(chalk.magenta('HOLA', 'MUNDO!', 'Foo', 'bar', 'biz', 'baz'));

// Nest styles
log(chalk.whiteBright('HOLA', chalk.underline.bgBlue('MUNDO') + '!'));

// Nest styles of the same type even (color, underline, background)
log(chalk.green(
	'Soy una linea verde ' +
	chalk.blue.underline.bold('mezclada con azul y subrayada') +
	' per vuelvo al verde xd!'
));

// ES2015 template literal
log(`
CPU: ${chalk.red('90%')}
RAM: ${chalk.green('40%')}
DISK: ${chalk.yellow('70%')}
`);


// Use RGB colors in terminal emulators that support it.
log(chalk.keyword('orange')('soy un texto de color predefinido'));
log(chalk.rgb(123, 45, 67).underline('soy un texto de color con rgb(123, 45, 67) '));
log(chalk.hex('#DEADED').bold('soy un texto de color con HEXADECIMAL!'));


//Easily define your own themes:
const error = chalk.bold.red;
const warning = chalk.keyword('orange');
console.log(error('Error!'));
console.log(warning('Warning!'));

//Take advantage of console.log string substitution:
const name = 'Sindre';
console.log(chalk.green('HOLA %s'), name);
//el %s hace que toda la linea sea verde




//VARIABLES DE ENTORNO
var env = require("./env.environment.json");

//la palabra reservada process.env.NODE_ENV, esto te traerá en qué ambiente se encuentra 
//corriendo tu aplicación. Con el ambiente ya podemos obtener las variables que le corresponden.

var node_env = process.env.NODE_ENV || 'development';

var varialbles = env[node_env];

//podremos utilizar las variables creadas
console.log(`El puerto de desarrollo es: ${varialbles.PORT}`);



//en vez de nodemos va npm run-script dev 