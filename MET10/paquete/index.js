
//npm login
//npm whoami
//npm publish (publicacion)
//npm version patch o  minor o major (actualizacion)

var env = require("./env.environment.json");

//obtener el valor del enviroment configurado
console.log(env.development.SERVERURL);

//obtener el environment del servidor, configurar variable de entorno en la maquina
//no aparece nada porque hay que configurarlo en variables de entorno
var node_env = process.env.NODE_ENV;

console.log('environment mi maquina',node_env) // dev

var node_env_local = process.env.NODE_ENV || 'development';

//sacar el valor del objeto
var variables = env[node_env_local];
console.log(`El puerto de desarrollo es: ${variables.PORT} paquete actualizado por el momento y su server url es ${variables.SERVERURL}`);
