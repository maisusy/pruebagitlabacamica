const fs = require('fs');

function leer_file(file){
    return fs.readFileSync(file,"utf-8");
}

exports.leer_file = leer_file;