// npm i BLABLA para instalar paquetes que quiera,ira a la carpeta node_modules

//npm!  Este comando es el encargado de leer todas las dependencias del archivo package.json e 
//instalar todos los módulos necesarios para que la aplicación corra de igual manera que en nuestro entorno de desarrollo.

var env = require("./env.variables.json");
//PARTE DEL CHALLENGE 
const chalk = require('chalk');


//la palabra reservada process.env.NODE_ENV, esto te traerá en qué ambiente se encuentra 
//corriendo tu aplicación. Con el ambiente ya podemos obtener las variables que le corresponden.

var node_env = process.env.NODE_ENV || 'development';

var varialbles = env[node_env];

//podremos utilizar las variables creadas
console.log(`El puerto de desarrollo es: ${varialbles.PORT}`);



//CHALLANGE 

const log = console.log;

// Combine styled and normal strings
log(chalk.green('HOLA') + ' MUNDO' + chalk.red('!'));

// Compose multiple styles using the chainable API
log(chalk.white.bgRed.bold('HOLA MUNDO!'));

// Pass in multiple arguments
log(chalk.magenta('HOLA', 'MUNDO!', 'Foo', 'bar', 'biz', 'baz'));

// Nest styles
log(chalk.whiteBright('HOLA', chalk.underline.bgBlue('MUNDO') + '!'));

// Nest styles of the same type even (color, underline, background)
log(chalk.green(
	'Soy una linea verde ' +
	chalk.blue.underline.bold('mezclada con azul') +
	' per vuelvo al verde xd!'
));

// ES2015 template literal
log(`
CPU: ${chalk.red('90%')}
RAM: ${chalk.green('40%')}
DISK: ${chalk.yellow('70%')}
`);


// Use RGB colors in terminal emulators that support it.
log(chalk.keyword('orange')('soy un texto de color predefinido'));
log(chalk.rgb(123, 45, 67).underline('soy un texto de color con rgb(123, 45, 67) '));
log(chalk.hex('#DEADED').bold('soy un texto de color con HEXADECIMAL!'));


//Easily define your own themes:
const error = chalk.bold.red;
const warning = chalk.keyword('orange');
console.log(error('Error!'));
console.log(warning('Warning!'));

//Take advantage of console.log string substitution:
const name = 'Sindre';
console.log(chalk.green('HOLA %s'), name);
//el %s hace que toda la linea sea verde